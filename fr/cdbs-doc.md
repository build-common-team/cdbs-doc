Préface
=======

Cette documentation décrit ce que nous avons réussi à apprendre de
l'utilisation de CDBS, avec le plus de détails possible. Néanmoins, nous
n'utilisons pas le jeu complet de fonctionnalités disponibles, et
certaines parties de cette documentation ont été écrites par pur soucis
pratique et d'exhaustivité.

Please note some examples in this documentation contain folded content
which is necessary to keep the pages at a reasonnable width; take care
to unfold them when necessary before using them (eg: `debian/control`
content must not be folded or build will fail or result be incorrect).

If you find mistakes or missing information, feel free to contact Marc
Dequènes (Duck) <duck@duckcorp.org>.

Introduction
============

Un peu d'histoire
-----------------

CDBS a été écrit par Jeff Bailey et Colin Waters en mars 2003, plus tard
rejoins pas 4 autre développeurs.

Des informations simples peuvent être trouvées sur leur [page de
projet](http://alioth.debian.org/projects/build-common/). Dans le paquet
est fourni [un petit jeu
d'exemples](http://cvs.alioth.debian.org/cgi-bin/cvsweb.cgi/cdbs/examples/?cvsroot=build-common)
(aussi disponible dans le paquet ici : /usr/share/doc/cdbs/examples/).

Depuis que nous expérimentions CDBS, il était devenu évident que le
manque de documentation nous empêchait de l'utiliser plus largement dans
nos paquets. Alors nous avons commencé à écrire quelques notes sur
l'utilisation de CDBS, qui ont rapidement grossi jusqu'à devenir
plusieurs pages. Cette documentation est une version révisée de [la page
wiki originale de la
DuckCorp](https://wiki.duckcorp.org/DebianPackagingTutorial_2fCDBS).

Pourquoi CDBS ?
---------------

CDBS is designed to simplify the maintainer's work so that they only
need to think about packaging and not maintaining a `debian/rules` file
that keeps growing bigger and more complicated. So CDBS can handle for
you most of common rules and detect some parts of your configuration.

CDBS n'utilise que des règles simples de makefile et est aisément
extensible en utilisant des classes. Des classes pour gérer les
autotools, l'application de rustines aux sources, les programmes GNOME,
l'installation de Python, et bien d'autres sont disponibles.

Les avantages de CDBS :

-   short, readable and efficient `debian/rules`

-   automatise l'usage de debhelper et des autotools pour que vous
    n'ayez pas à vous préoccuper de cette déplaisante et répétitive
    tache.

-   le mainteneur peut se concentrer sur les vrais problèmes de paquets
    car CDBS vous aide sans vous limiter

-   les classes utilisées dans CDBS ont été bien testées donc vous
    faites usage de règles sans erreurs et évitez de faire de sales
    correctifs pour résoudre des problèmes banals

-   migrer vers CDBS est facile

-   can be used to generate Debian files (like `debian/control` for
    GNOME Team Uploaders inclusion)

-   CDBS est facilement extensible

-   Il |70\>\< !!!

Premiers pas
============

Convertir un paquet à CDBS
--------------------------

Converting to CDBS is easy; A simple `debian/rules` for a C/C++ software
with no extra rules would be written as this : \#!/usr/bin/make -f
include /usr/share/cdbs/1/rules/debhelper.mk include
/usr/share/cdbs/1/class/autotools.mk

No, i'm not joking, this is sufficient to handle autotools management,
like updating `config.{guess|sub}`, cleanup temp files after build and
launch all common debhelper stuff.

Just use compat level 7 (or lower if needed, but not lower than 4 or it
may not work), create your `.install`, ``.info, etc as you usually do
with `dh_` scripts, and CDBS would call them if necessary, autodetecting
a lot of things. In case of a missing compat information, CDBS would
create a `debian/compat` file with compatibility level 7. If you are
using an obsolete `DH_COMPAT` variable in your `debian/rules`, you
should get rid of it. In this case, or in case you would like CDBS not
to create a `debian/compat` file, you can disable this feature by
setting `DEB_DH_COMPAT_DISABLE` to a non-void value.

> **Important**
>
> If `debian/control` management is activated (see below), build
> dependency on `cdbs` is automatically added, if not, you will have to
> do it yourself.

> **Warning**
>
> Beware your working directory *MUST NOT* have spaces or CDBS would
> probably fail; see [\#306941](http://bugs.debian.org/306941)

Réglages de base et variables disponibles
-----------------------------------------

Remember you can get the pkg directory using the `CURDIR` variable.

You can change common build parameters this way : \# where sources are
DEB\_SRCDIR = \$(CURDIR)/src \# in which directory to build
DEB\_BUILDDIR = \$(DEB\_SRCDIR)/build \# in which directory to install
the sofware DEB\_DESTDIR = \$(CURDIR)/plop/

Some various variables you can use in `debian/rules` :

  ------------------------ -----------------------------------------------------------------------
  `DEB_SOURCE_PACKAGE`     nom du paquet source
  `DEB_VERSION`            version Debian complète
  `DEB_NOEPOCH_VERSION`    version Debian sans epoch
  `DEB_UPSTREAM_VERSION`   version amont
  `DEB_ISNATIVE`           non-vide si le paquet est natif
                           
  `DEB_ALL_PACKAGES`       liste de tous les paquets binaires
  `DEB_INDEP_PACKAGES`     liste des paquets binaires indépendants de l'architecture
  `DEB_ARCH_PACKAGES`      liste des paquets binaires dépendants de l'architecture
  `DEB_PACKAGES`           liste des paquets binaires normaux (non-udeb)
  `DEB_UDEB_PACKAGES`      liste des paquets binaires udeb, s'ils existent
                           
  `DEB_HOST_GNU_TYPE`      type GNU de la machine hôte
  `DEB_HOST_GNU_SYSTEM`    partie système de type GNU de la machine hôte
  `DEB_HOST_GNU_CPU`       partie CPU du type GNU de la machine hôte
  `DEB_HOST_ARCH`          nom de l'architecture Debian sur la machine hôte
  `DEB_HOST_ARCH_CPU`      partie CPU du nom de l'architecture Debian sur la machine hôte
  `DEB_HOST_ARCH_OS`       partie OS du nom de l'architecture Debian sur la machine hôte
                           
  `DEB_BUILD_GNU_TYPE`     type GNU pour cette construction
  `DEB_BUILD_GNU_SYSTEM`   partie système du type GNU pour cette construction
  `DEB_BUILD_GNU_CPU`      partie CPU du type GNU pour cette construction
  `DEB_BUILD_ARCH`         nom de l'architecture Debian pour cette construction
  `DEB_BUILD_ARCH_CPU`     partie CPU du nom de l'architecture Debian pour cette construction
  `DEB_BUILD_ARCH_OS`      partie OS du nom de l'architecture Debian pour cette construction
                           
  `DEB_ARCH`               vieux nom de l'architecture Debian
                           */!\\ deprecated, only use to provide backward compatibility /!\\*
                           (voir la page de manuel de dpkg-architecture pour plus d'information)
  ------------------------ -----------------------------------------------------------------------

  : Common variables available in `debian/rules`

Règles de construction simples et sur-mesures
---------------------------------------------

> **Warning**
>
> Prenez soin d'ajouter les règles *après* les inclusions
> CDBS nécessaires.

Suppose you want custom rules for the source package foo, creating foo
(arch-dep) and foo-data (arch-indep), you simply need to add some lines
to `debian/rules`.

To add pre-configure actions : makebuilddir/foo:: ln -s plop plop2

To add post-configure actions : configure/foo:: sed -ri 's/PLOP/PLIP/'
Makefile configure/foo-data:: touch src/z.xml */!\\ in this case we are
talking about package configuration and NOT about a configure script
made with autotools.*

To add post-build actions : build/foo:: /bin/bash debian/scripts/toto.sh
build/foo-data:: \$(MAKE) helpfiles

To add post-install actions : install/foo:: cp debian/tmp/myfoocmd
debian/foo/foocmd find debian/foo/ -name "CVS" -depth -exec rm -rf {}
\\; install/foo-data:: cp data/\*.png
debian/foo-data/usr/share/foo-data/images/ dh\_stuff -m ipot -f plop.bz3
debian/foo-data/libexec/

To add post deb preparation actions : binary/foo:: strip
--remove-section=.comment --remove-section=.note --strip-unneeded \\
debian/foo/usr/lib/foo/totoz.so

To add pre-clean actions : cleanbuilddir/foo:: rm -f debian/fooman.1

Options de compilation usuelles
-------------------------------

Default optimization is set using `DEB_OPT_FLAG` which default to "-O2";
you can override it. `CFLAGS` and `CXXFLAGS` are set to "-g -Wall
\$(DEB\_OPT\_FLAG)", CPPFLAGS is untouched from environment, but you can
override these settings on a per-package basis using `CFLAGS_`,
`CXXFLAGS_`, and `CPPFLAGS_` variables.

`DEB_BUILD_OPTIONS` is a well known Debian environment variable, not a
CDBS one, containing special build options (a comma-separated list of
keywords). CDBS does check `DEB_BUILD_OPTIONS` to take these options
into account; see details in each class.

Les astuces de Debhelper
------------------------

### Ne pas s'occuper de Debhelper

Oui, CDBS fait presque tout pour vous :-) .

Just add this line to the beginning of your `debian/rules` file :
include /usr/share/cdbs/1/rules/debhelper.mk

CDBS debhelper rules handle the following Debhelper scripts for each
binary package automatically :

  ---------------- ------------------------ ---------------------- ----------------------- ------------------
  `dh_builddeb`    `dh_installcatalogs`     `dh_installdocs`       `dh_installlogrotate`   `dh_link`
  `dh_clean`       `dh_installchangelogs`   `dh_installemacsen`    `dh_installman`         `dh_makeshlibs`
  `dh_compress`    `dh_installcron`         `dh_installexamples`   `dh_installmenu`        `dh_md5sums`
  `dh_fixperms`    `dh_installdeb`          `dh_installinfo`       `dh_installmime`        `dh_perl`
  `dh_prep`        `dh_gencontrol`          `dh_installdebconf`    `dh_installinit`        `dh_installpam`
  `dh_shlibdeps`   `dh_install`             `dh_installdirs`       `dh_installlogcheck`    `dh_installudev`
  `dh_strip`
  ---------------- ------------------------ ---------------------- ----------------------- ------------------

  : Scripts Debhelper communément gérés

Other Debhelper scripts can be handled in specific classes or may be
called in custom rules.

> **Important**
>
> If `debian/control` management is activated (see below), build
> dependency on `debhelper` is automatically added, if not, you will
> have to do it yourself.
>
> Having a versioned dependency on `debhelper` is recommended as it will
> ensure people will use the version providing the necessary features
> (CDBS `debian/control` management will do it).

### Paramètres de Debhelper

The following parameters allow debhelper calls customization while most
common calls are still handled without writing any rule. Some of them
apply on all binary packaged, like `DEB_INSTALL_DOCS_ALL`, and some
apply only to a specific package, like `DEB_SHLIBDEPS_LIBRARY_` (where
pkg is the name of the binary package). Read the comments in
`/usr/share/cdbs/1/rules/debhelper.mk` for a complete listing. Some
non-exhaustive examples follow.

To specify a tight dependency on a package containing shared libraries:
DEB\_DH\_MAKESHLIBS\_ARGS\_libfoo := -V"libfoo (\>= 0.1.2-3)"
DEB\_SHLIBDEPS\_LIBRARY\_arkrpg := libfoo
DEB\_SHLIBDEPS\_INCLUDE\_arkrpg := debian/libfoo/usr/lib/

To install a changelog file with an uncommon name as
`ProjectChanges.txt.gz`: DEB\_INSTALL\_CHANGELOGS\_ALL :=
ProjectChanges.txt

To avoid compressing files with i`.py` extension :
DEB\_COMPRESS\_EXCLUDE := .py

To register a debug library package libfoo-dbg for libfoo (which needs
unstripped `.so`) in compat mode 4: DEB\_DH\_STRIP\_ARGS :=
--dbg-package=libfoo Starting from compat mode 5, CDBS automatically
detect -dbg packages and pass the needed arguments to dh\_strip;
`DEB_DH_STRIP_ARGS` can still be useful to pass additional parameters
like excluded items (`--exclude=`).

Perl-specific debhelper options (dh\_perl call is always performed): \#
Add a space-separated list of paths to search for perl modules
DEB\_PERL\_INCLUDE := /usr/lib/perl-z \# Like the above, but for the
'libperl-stuff' package DEB\_PERL\_INCLUDE\_libperl-stuff :=
/usr/lib/perl-plop \# Overrides options passed to dh\_perl
DEB\_DH\_PERL\_ARGS := -d

To avoid loosing temporary generated files in dh\_clean processing
(rarely useful): \# files containing these pattern would not be deleted
\# (beware CDBS changelog has a typo while highlighting new
DEB\_CLEAN\_EXCLUDE\*S\* feature) DEB\_CLEAN\_EXCLUDE := precious keep

### Règles personnalisées de construction pour debhelper

Les règles de CDBS pour debhelper ajoutent aussi des règles plus
judicieuses pour la construction du paquet.

To add post deb preparation (including debhelper stuff) actions :
binary-install/foo:: chmod a+x debian/foo/usr/bin/pouet

To add post clean actions : clean:: rm -rf plop.tmp

D'autres règles existent, mais nous ne les avons pas testé :

-   binary-strip/foo (appelé après la suppression des symboles de
    débogage)

-   binary-fixup/foo (appelé après la compression et la correction des
    permissions)

-   binary-predeb (appelé juste avant la construction du .deb)

Tâches courantes
================

Appliquer une rustine (en utilisant simple-patchsys)
----------------------------------------------------

Tout d'abord, l'application de rustines directement dans les sources est
MAL™, donc vous avez besoin d'une façon d'appliquer les rustines sans
toucher aux fichiers. Ces règles, inspirées pas le système Dpatch, sont
très similaires et efficaces. Tout ce que vous avez à connaître est
l'usage de diff et patch, CDBS s'occupe du reste.

C'est très dur, donc écoutez attentivement et préparez vous pour
l'examen.

First, add this line to your `debian/rules` : include
/usr/share/cdbs/1/rules/simple-patchsys.mk And then use it !

Create the directory `debian/patches` and put your patches in it. Files
should be named so as to reflect in which order they have to be applied,
and must finish with the `.patch` or `.diff` suffix. The class would
take care of patching before configure and unpatch after clean. It is
possible to use patch level 0 to 3, and CDBS would try them and use the
right level automatically. The system can handle compressed patch with
additional `.gz` or `.bz2` suffix and uu-encoded patches with additional
`.uu` suffix.

You can customize the directories where patches are searched, and the
suffix like this : (defaults are: .diff .diff.gz .diff.bz2 .diff.uu
.patch .patch.gz .patch.bz2 .patch.uu) DEB\_PATCHDIRS :=
debian/mypatches DEB\_PATCH\_SUFFIX := .plop

In case of errors when applying, for example
`debian/patches/01_hurd_ftbfs_pathmax.patch`, you can read the log for
this patch in `debian/patches/01_hurd_ftbfs_pathmax.patch.level-0.log`
('0' because a level 0 patch).

> **Important**
>
> If `debian/control` management is activated (see below), build
> dependency on `patchutils` is automatically added, if not, you will
> have to do it yourself.

Appliquer des rustines (en utilisant dpatch)
--------------------------------------------

To use Dpatch as an alternative to the CDBS included patch system, just
add his line to your `debian/rules` : include
/usr/share/cdbs/1/rules/dpatch.mk \# needed to use the dpatch tools
(like dpatch-edit-patch) include /usr/share/dpatch/dpatch.make Now you
can use Dpatch as usual and CDBS would call it automatically.

> **Warning**
>
> Vous devriez inclure dpatch.mk *APRÈS* autotools.mk ou gnome.mk pour
> que l'extension dpatch fonctionne correctement.

> **Important**
>
> If `debian/control` management is activated (see below), build
> dependency on `dpatch` and `patchutils` is automatically added, if
> not, you will have to do it yourself.

Appliquer des rustines (en utilisant quilt)
-------------------------------------------

To use [Quilt](http://savannah.nongnu.org/projects/quilt) as an
alternative to the CDBS included patch system, just add his line to your
`debian/rules` : include /usr/share/cdbs/1/rules/patchsys-quilt.mk Now
you can use Quilt as usual and CDBS would call it automatically.

> **Important**
>
> If `debian/control` management is activated (see below), build
> dependency on `quilt` and `patchutils` is automatically added, if not,
> you will have to do it yourself.

Gestion automatique des tarball
-------------------------------

To use the CDBS tarball system, just add his line to your
`debian/rules`, and specify the name of the top directory of the
extracted tarball : include /usr/share/cdbs/1/rules/tarball.mk
DEB\_TAR\_SRCDIR := foosoft CDBS will recognize tarballs with the
following extensions: .tar .tgz .tar.gz .tar.bz .tar.bz2 .zip

The tarball location is autodetected if in the top source directory, or
can be specified : DEB\_TARBALL :=
\$(CURDIR)/tarballdir/mygnustuff\_beta-1.2.3.tar.gz

CDBS will handle automatic uncompression and cleanups, automagically set
`DEB_SRCDIR` and `DEB_BUILDDIR` for you, and integrate well with other
CDBS parts (like autotools class).

Moreover, if you want sources to be cleaned up from dirty SCM-specific
dirs and file, just add this at the top of your `debian/rules`, before
any include : DEB\_AUTO\_CLEANUP\_RCS := yes

> **Warning**
>
> The `DEB_AUTO_CLEANUP_RCS` feature has been removed for no good reason
> since version 0.4.39. Feel free to bugreport if you want it
> resurrected.

> **Important**
>
> If needed, and if `debian/control` management is activated (see
> below), build dependency on `bzip2` or `unzip` is automatically added,
> if not, you will have to do it yourself.

Personnalisation avancée
========================

`debian/control` management
---------------------------

> **Caution**
>
> Automatic `debian/control` generation using any tool is permitted into
> Debian as long as it is triggered manually by the developer and the
> latter checks the result carefully.
>
> Autogenerating `debian/control` without any human intervention could
> be harmful in some ways detailed in
> [\#311724](http://bugs.debian.org/311724). This is not allowed in
> Debian.
>
> We then urge you to avoid using `DEB_AUTO_UPDATE_DEBIAN_CONTROL`
> directly and instead invoke the autogeneration rules manually after
> you modified `debian/control.in` (this way users or buildds wouldn't
> have different Build-Depends when building, avoiding many problems).
> Do not forget to proofread the result before any upload.
>
> Manual `debian/control` regeneration:
> DEB\_AUTO\_UPDATE\_DEBIAN\_CONTROL=yes fakeroot debian/rules clean

Cette fonction permet :

-   à CDBS de gérer automatiquement certains « build-related
    Build-Depends »

-   l'utilisation de commandes shell intégrées.

-   use of CPU and System criterias to specify architecture
    (*EXPERIMENTAL*)

« Build-related Build-Depends » sont des dépendances introduites avec
l'utilisation de certaines fonctions CDBS, ou de besoins détectés
automatiquement.

Embedded shell commands allows including hacks like : Build-Depends:
libgpm-dev [\`type-handling any linux-gnu\`]

CPU and System criterias implements support for Cpu/System fields, as a
replacement for the Architecture field (which is to be implemented in
dpkg in the long term, but still *EXPERIMENTAL*). Here is an exemple,
before : Architecture: all and after : Cpu: all System: all If these
fields are used, it is also possible to include special tags to easily
take advantage of the `type-handling` tool, like in this example :
Build-Depends: @cdbs@, procps [system: linux], plop [cpu: s390] (look at
the `type-handling` package documentation, for more information)

1.  Rename `debian/control` into `debian/control.in`.

2.  Replace cdbs / debhelper / ... Build-Depends with @cdbs@ in your
    `debian/control.in` like this :

        Build-Depends-Indep: @cdbs@, python-dev (>= 2.3), python-soya (>= 0.9), \
           python-soya (<< 0.10), python-openal(>= 0.1.4-4), gettext
                                

3.  Then manually (re)generate `debian/control` as explained above (see
    the caution part).

Utilisation de la classe Makefile
---------------------------------

Cette classe est destinée à ceux qui ont uniquement un fichier Makefile
(aucun autotools disponible) pour construire le programme. Vous avez
uniquement besoin de quatre règles dans le Makefile :

-   une pour nettoyer le répertoire de construction (c'est-à-dire
    « mrproper »)

-   une pour construire votre logiciel (c'est-à-dire « myprog »)

-   une pour vérifier que votre logiciel fonctionne correctement
    (c'est-à-dire « check »)

-   une pour installer votre logiciel (c'est-à-dire « install »)

Pour être honnête, la présence des règles d'installation n'est pas
essentielle, mais cela aide toujours beaucoup lorsque vous les avez.

The first operation, is to write the `debian/rules`. First, we add the
include lines: include /usr/share/cdbs/1/class/makefile.mk

Now, it remains to tell cdbs the name of our four Makefile rules. For
the previous examples it gives: DEB\_MAKE\_CLEAN\_TARGET := mrproper \#
if you detect authors's loss of sanity, tell CDBS not to try running the
nonexistent clean rule, and do the job yourself in `debian/rules`
DEB\_MAKE\_CLEAN\_TARGET := DEB\_MAKE\_BUILD\_TARGET := myprog
DEB\_MAKE\_INSTALL\_TARGET := install DESTDIR=\$(CURDIR)/debian/tmp/ \#
no check for this software DEB\_MAKE\_CHECK\_TARGET := \# allow changing
the makefile filename in case of emergency exotic practices
DEB\_MAKE\_MAKEFILE := MaKeFiLe \# example when changing environnement
variables is necessary : DEB\_MAKE\_ENVVARS :=
CFLAGS="-fomit-frame-pointer"

`DEB_BUILD_OPTIONS` is checked for the following options :

-   noopt: utilise -O0 au lieu de -O2

-   nocheck: skip the check rule

If your Makefile doesn't support the `DESTDIR` variable, take a look in
it and find the variable responsible for setting installation directory.
If you don't find some variable to do this, you'll have to patch the
file...

C'est tout :)

Utilisation de la classe Autotools
----------------------------------

This class is able to use configure scripts and makefiles generated with
autotools (and possibly libtool). All rules are called automatically and
clean rules to remove generated files during build are also added. This
class in fact improves the makefile class to support autotools features
and provide good defaults.

To use it, just add this line to your `debian/rules` include
/usr/share/cdbs/1/class/autotools.mk

CDBS automatically handles common flags to pass to the configure script,
but it is possible to give some extra parameters :
DEB\_CONFIGURE\_EXTRA\_FLAGS := --with-ipv6 --with-foo

If the build system uses non-standard configure options you can override
CDBS default behavior : COMMON\_CONFIGURE\_FLAGS := --program-dir=/usr
(notice that `DEB_CONFIGURE_EXTRA_FLAGS` would still be appended)

If some specific environnement variables need to be setup, use :
DEB\_CONFIGURE\_SCRIPT\_ENV += BUILDOPT="someopt"

> **Warning**
>
> Prefer use of += instead of := not to override other environment
> variables (CC / CXX / CFLAGS / CXXFLAGS / CPPFLAGS / LDFLAGS)
> propagated in the CDBS default.

CDBS will automatically update `config.sub`, `config.guess`, and
`config.rpath` before build and restore the old ones at clean stage
(even if using the tarball system). If needed, and if `debian/control`
management is activated, `autotools-dev` and/or `gnulib` will then be
automatically added to the build dependencies (needed to find updated
versions of the files).

If the program does not use the top source directory to store autoconf
files, you can teach CDBS where it is to be found : DEB\_AC\_AUX\_DIR =
\$(DEB\_SRCDIR)/autoconf

CDBS can be asked to update libtool, autoconf, and automake files, but
this behavior is likely to break the build system and is *STRONGLY*
discouraged. Nevertheless, if you still want this feature, set the
following variables :

-   `DEB_AUTO_UPDATE_LIBTOOL`: `pre` to call libtoolize, or `post` to
    copy system-wide `libtool` after configure is done

-   `DEB_AUTO_UPDATE_ACLOCAL`: `aclocal` version to use

-   `DEB_AUTO_UPDATE_AUTOCONF`: `autoconf` version to use

-   `DEB_AUTO_UPDATE_AUTOHEADER`: `autoheader` version to use

-   `DEB_AUTO_UPDATE_AUTOMAKE`: `automake` version to use

-   `DEB_AUTOMAKE_ARGS`: extra arguments to `automake` call

(les dépendances de construction correspondantes seront ajoutées
automatiquement)

The following make parameters can be overridden : \# these are the
defaults CDBS provides DEB\_MAKE\_INSTALL\_TARGET := install
DESTDIR=\$(DEB\_DESTDIR) DEB\_MAKE\_CLEAN\_TARGET := distclean
DEB\_MAKE\_CHECK\_TARGET := \# example to work around dirty makefile
DEB\_MAKE\_INSTALL\_TARGET := install prefix=\$(CURDIR)/debian/tmp/usr
\# example with unexistant install rule for make
DEB\_MAKE\_INSTALL\_TARGET := \# example to activate check rule
DEB\_MAKE\_CHECK\_TARGET := check \# overriding make-only environnement
variables : \# (should never be necessary in a clean build system) \#
(example borrowed from the bioapi package) DEB\_MAKE\_ENVVARS :=
"SKIPCONFIG=true"

`DEB_BUILD_OPTIONS` is checked for the following options :

-   noopt: utilise -O0 au lieu de -O2

-   nocheck: skip the check rule

If you are using CDBS version \< 0.4.39, it automatically cleans
autotools files generated during build (`config.cache`, `config.log`,
and `config.status`). Since version 0.4.39, CDBS leave them all
considering it is not his job to correct an upstream buildsys
misbehavior (but you may remove them in the clean rule if necessary
before you get the issue solved by authors).

Utilisation de la classe Perl
-----------------------------

This class can manage standard perl build and install with MakeMaker
method.

To use this class, add this line to your `debian/rules` : include
/usr/share/cdbs/1/class/perlmodule.mk Optionally, it can take care of
using dh\_perl, depending the debhelper class is declared before the
perl class or not.

Install path defaults to 'first\_pkg/usr' where first\_pkg is the first
package in `debian/control`.

You can customize build options like this : \# change MakeMaker defaults
(should never be usefull) DEB\_MAKE\_BUILD\_TARGET := build-all
DEB\_MAKE\_CLEAN\_TARGET := realclean DEB\_MAKE\_CHECK\_TARGET :=
DEB\_MAKE\_INSTALL\_TARGET := install PREFIX=debian/stuff \# add custom
MakeMaker options DEB\_MAKEMAKER\_USER\_FLAGS := --with-ipv6

Common makefile or general options can still be overrided:
`DEB_MAKE_ENVVARS`, `DEB_BUILDDIR` (must match `DEB_SRCDIR` for Perl)

Référez-vous aux options de debhelper spécifiques à Perl décrites
ci-dessus.

> **Important**
>
> If `debian/control` management is activated (see below), build
> dependency on `perl` is automatically added, if not, you will have to
> do it yourself.

Utilisation de la classe Python
-------------------------------

This class can manage common Python builds using
[distutils](http://docs.python.org/distutils/) automatically.

> **Warning**
>
> Since 0.4.53, this class does not handle old-policy packages
> (pre-Etch) anymore.

With the new policy all versionned packages (pythonver-app) are
collapsed into a single package (python-app). The class is able to move
python scripts and .so files in the new locations automatically. You can
use the auto control file generation feature to ensure your
Build-Depends are set correctly for the new needed tools.

To use this class, add these lines to your `debian/rules` : \# select
the python system you want to use : pysupport or pycentral \# (this MUST
be done before including the class) DEB\_PYTHON\_SYSTEM = pysupport
include /usr/share/cdbs/1/class/python-distutils.mk The class also takes
care of calling dh\_pysupport or dh\_pycentral.

You can customize build options like this : \#\#\#\#\#\#\#\#\#\#\#\#\#\#
These variables are additions for the new policy \# packages holding the
collapsed content of all supported versions \# CDBS defaults to the
first non -doc/-dev/-common package listed in "debian/control" \# this
variable allows to override automatic detection
DEB\_PYTHON\_MODULE\_PACKAGES = mypyapp \# list of private modules
private directoris (needed to automatically handle bytecompilation)
DEB\_PYTHON\_PRIVATE\_MODULES\_DIRS = /usr/share/mypyapp/my-pv-module \#
overrides the default Python installation root directory
DEB\_PYTHON\_DESTDIR = \$(CURDIR)/debian/python-stuff
\#\#\#\#\#\#\#\#\#\#\#\#\#\# These variables have not changed (same in
the old and new policy) \# change the Python build script name (default
is 'setup.py') DEB\_PYTHON\_SETUP\_CMD := install.py \# clean options
for the Python build script DEB\_PYTHON\_CLEAN\_ARGS = -all \# build
options for the Python build script DEB\_PYTHON\_BUILD\_ARGS =
--build-base="\$(DEB\_BUILDDIR)/specific-build-dir" \# common additional
install options for all binary packages \# ('--root' option is always
set) DEB\_PYTHON\_INSTALL\_ARGS\_ALL = --no-compile --optimize --force

You may use some read-only (meaning you MUST NOT alter them) variables
in your `debian/rules` :

-   `cdbs_python_current_version`: current python version number
    (defined only if selected package is a module)

-   `cdbs_python_build_versions`: list of space separated version
    numbers for which the selected module/extension is gonna be built

-   `cdbs_python_destdir`: Python installation root directory (works
    even when you didn't used `DEB_PYTHON_DESTDIR`

-   `cdbs_python_packages`: list of all Python packages

-   `cdbs_python_arch_packages`: list of all Python architecture
    dependent packages

-   `cdbs_python_indep_packages`: list of all Python architecture
    independent packages

Complete `debian/rules` example using python-support for a module
(editobj): \#!/usr/bin/make -f \# -\*- mode: makefile; coding: utf-8
-\*- include /usr/share/cdbs/1/rules/debhelper.mk DEB\_PYTHON\_SYSTEM =
pysupport include /usr/share/cdbs/1/class/python-distutils.mk include
/usr/share/cdbs/1/rules/patchsys-quilt.mk DEB\_COMPRESS\_EXCLUDE := .py
\$(patsubst %,install/%,\$(cdbs\_python\_packages)) mv
debian/\$(cdbs\_curpkg)/usr/lib/python\*/site-packages/editobj/icons \\
debian/\$(cdbs\_curpkg)/usr/share/\$(cdbs\_curpkg) \$(patsubst
%,binary-install/%,\$(cdbs\_python\_packages)) find
debian/\$(cdbs\_curpkg)/usr/share/ -type f -exec chmod -R a-x {} \\;

> **Warning**
>
> Do not use the `DEB_PYTHON_MODULE_PACKAGE` variable anymore, it has
> been obsoleted. Use the `DEB_PYTHON_MODULE_PACKAGES` to force the list
> of module packages, or `cdbs_python_packages` variable in rules
> generation.
>
> Do not use the `cdbs_python_support_path` and
> `cdbs_python_module_arch` variables anumore, they have been obsoleted.
> `cdbs_python_support_path` is not usefull anymore, just install files
> in the standard way and python-support would do the rest (messing into
> python-support internals was only a workaround when it was
> incomplete). `cdbs_python_module_arch` can easily be replace by a rule
> rewrite or a membership test using `cdbs_python_arch_packages` or
> `cdbs_python_indep_packages`.

Utilisation de la classe Ruby setup.rb
--------------------------------------

Cette classe peut gérer automatiquement les programmes d'installation
setup.rb usuels.

To use this class, add this line to your `debian/rules` : include
/usr/share/ruby-pkg-tools/1/class/ruby-setup-rb.mk Optionally, it can
take care of using dh\_rdoc, to generate and install Rdoc documentation,
depending the debhelper class is declared before the ruby setup.rb class
or not.

Most ruby packages are architecture all, and then don't need being build
for multiple ruby versions; your package should then be called
'libfoo-ruby' or 'foo' and CDBS would automatically use the current
Debian ruby version to build it. If your package contains a compiled
part or a binding to an external lib, then you will have packages named
'libfoo-ruby1.6', 'libfoo-ruby1.8', and so on, then CDBS would
automatically build each package with the corresponding ruby version. In
this case, don't forget to add a 'libfoo-ruby' convenience dummy package
depending on the current Debian ruby version. If you have documentation
you want split into a separate package, then call it 'libfoo-ruby-doc'.
If this is Rdoc documentation, you may want to include the debhelper
class, as explained before, to have it generated and installed
automagically.

You can customize build options like this : \# force using a specific
ruby version for build \# (should not be necessary) DEB\_RUBY\_VERSIONS
:= 1.9 \# use ancestor DEB\_RUBY\_SETUP\_CMD := install.rb \# config
options for the ruby build script \# (older setup.rb used --site-ruby
instead of --siteruby) DEB\_RUBY\_CONFIG\_ARGS =
--site-ruby=/usr/lib/ruby/1.9-beta

Utilisation de la classe de l'équipe Debian Ruby Extras
-------------------------------------------------------

Si vous appartenez à l'équipe Ruby Extras, ou que vous avez une équipe
comme « uploaders », et que maintenir la liste des développeurs vous
ennuie, cette classe est faite pour vous.

To use this class, add this line to your `debian/rules` : include
/usr/share/ruby-pkg-tools/1/rules/uploaders.mk

Rename your `debian/control` file to `debian/control.in` and run the
clean rule (`./debian/rules clean`) to regenerate the `debian/control`
file, replacing the `@RUBY_TEAM@` tag with the list of developers
automatically.

Utilisation de la classe GNOME
------------------------------

This class adds a make environnement variable :
GCONF\_DISABLE\_MAKEFILE\_SCHEMA\_INSTALL = 1 (''This is necessary
because the Gconf schemas have to be registered at install time. In the
case of packaging, this registration cannot be done when building the
package, so this variable disable schema registration in `make
install`. This procedure if defered until gconftool-2 is called in
`debian/postinst` to register them, and in `debian/prerm` to unregister
them. The dh\_gconf script is able to add the right rules automatically
for you.'')

It can handle the following Debhelper scripts automagically :

  -------------- ------------ ------------ -------------------
  `dh_desktop`   `dh_gconf`   `dh_icons`   `dh_scrollkeeper`
  -------------- ------------ ------------ -------------------

  : Scripts Debhelper gérés par la classe GNOME

Moreover it adds some more clean rules to remove:

-   fichiers générés par intltool

-   scrollkeeper generated files (left over `.omf.out` files in `doc`
    and `help` directories)

To use it, just add this line to your `debian/rules`, after the
debhelper class include : include /usr/share/cdbs/1/class/gnome.mk

Pour de plus amples informations sur les règles d'empaquetage
spécifiques à GNOME, référez-vous à la [Politique d'empaquetage Debian
GNOME](http://alioth.debian.org/docman/view.php/30194/18/gnome-policy-20030502-1.html)
(en anglais).

Utilisation de la classe de l'équipe Debian GNOME
-------------------------------------------------

Si vous appartenez à l'équipe GNOME, ou si vous avez l'équipe comme
« uploaders », et que maintenir la liste des développeurs vous ennuie,
cette classe est faite pour vous.

To use this class, add this line to your `debian/rules` : include
/usr/share/gnome-pkg-tools/1/rules/uploaders.mk

Rename your `debian/control` file to `debian/control.in` and run the
clean rule (`./debian/rules clean`) to regenerate the `debian/control`
file, replacing the '@GNOME\_TEAM@' tag with the list of developers
automatically.

> **Warning**
>
> If you are using the `debian/control` file management described below,
> please note this class will override this feature To cope with this
> problem, allowing at least Build-Depends handling, use the following
> work-arround (until it is solved in a proper way) : \# deactivate
> `debian/control` file management \#DEB\_AUTO\_UPDATE\_DEBIAN\_CONTROL
> := yes \# ... \# includes and other stuff \# ... clean:: sed -i
> "s/@cdbs@/\$(CDBS\_BUILD\_DEPENDS)/g" debian/control \# other clean
> stuff

Utilisation de la classe KDE
----------------------------

To use this class, add this line to your `debian/rules` file : include
/usr/share/cdbs/1/class/kde.mk

CDBS exporte automatiquement les variables suivantes avec la valeur
correcte :

-   kde\_cgidir (/usr/lib/cgi-bin)

-   kde\_confdir (/etc/kde3)

-   kde\_htmldir (/usr/share/doc/kde/HTML)

`DEB_BUILDDIR`, `DEB_AC_AUX_DIR` and `DEB_CONFIGURE_INCLUDEDIR` are set
to KDE defaults.

Les fichiers suivants sont exclus de la compression :

-   .dcl

-   .docbook

-   -license

-   .tag

-   .sty

-   .el

(take care of them if you override the `DEB_COMPRESS_EXCLUDE` variable)

Il peut gérer des options de configuration spécifiques à KDE (sans
oublier de désactiver rpath et activer xinerama), définir correctement
le répertoire autotools, et lancer les règles make de manière adéquate.

You can enable APIDOX build by setting the `DEB_KDE_APIDOX` variable to
a non-void value.

you can enable the final mode build by setting `DEB_KDE_ENABLE_FINAL`
variable to a non-void value.

`DEB_BUILD_OPTIONS` is checked for the following options :

-   noopt: disable optimisations (and KDE final mode, overriding
    `DEB_KDE_ENABLE_FINAL`)

-   nostrip: enable KDE debug (and disable KDE final mode, overriding
    `DEB_KDE_ENABLE_FINAL`)

You can prepare the build using the 'buildprep' convenience target:
`fakeroot debian/rules buildprep` (which is in fact calling the dist
target of `admin/Makefile.common`).

Utilisation de la classe Ant
----------------------------

(Ant est un outil de construction basé sur java)

To use this class, add this include to your `debian/rules` and set the
following variables : include /usr/share/cdbs/1/class/ant.mk \#
Définissez soit un unique (JAVA\_HOME) ou de multiples
(JAVA\_HOME\_DIRS) emplacements java JAVA\_HOME := /usr/lib/kaffe \# ou
définissez JAVACMD si vous n'utilisez pas le chemin
« \<JAVA\_HOME\>/bin/java » par défaut \#JAVACMD := /usr/bin/java \#
Définissez un emplacement Ant ANT\_HOME := /usr/share/ant-cvs

You may add additional JARs like in the following example : \# list of
additional JAR files ('.jar' extension may be omited) \# (path must be
absolute of relative to '/usr/share/java') DEB\_JARS :=
/usr/lib/java-bonus/ldap-connector adml-adapter.jar

The property file defaults to `debian/ant.properties`.

You can provide additional JVM arguments using ANT\_OPTS. You can
provide as well additional Ant command line arguments using ANT\_ARGS
(global) and/or ANT\_ARGS\_pkg (for package pkg), thus overriding the
settings in `build.xml` and the property file.

CDBS will build and clean using defaults target from `build.xml`. To
override these rules, or run the install / check rules, set the
following variables to your needs : \# override build and clean target
DEB\_ANT\_BUILD\_TARGET = makeitrule DEB\_ANT\_CLEAN\_TARGET =
super-clean \# i want install and test rules to be run
DEB\_ANT\_INSTALL\_TARGET = install-all DEB\_ANT\_CHECK\_TARGET = check

`DEB_BUILD_OPTIONS` is checked for the following options :

-   noopt: définir l'option « compile.optimize » Ant à « false » (faux)

Vous devriez pouvoir obtenir plus d'informations sur ces outils de
construction basées sur java sur le [site web Ant
Apache](http://ant.apache.org/). (en anglais).

Utilisation de la classe HBuild
-------------------------------

(HBuild est le mini-distutils Haskell)

CDBS can take care of -hugs and -ghc packages: invoke `Setup.lhs`
properly for clean and install part.

To use this class, add this line to your `debian/rules` : include
/usr/share/cdbs/1/class/hbuild.mk

Vous devriez pouvoir obtenir plus d'informations sur les distutils
Haskell dans [ce fil de
discussion](http://www.haskell.org/pipermail/libraries/2003-July/001239.html)
(en anglais).

Using the CMake class
---------------------

This class is intended to handle build systems using
[CMake](http://www.cmake.org/).

To use this class, add this line to your `debian/rules` : include
/usr/share/cdbs/1/class/cmake.mk

This class is an extension of the Makefile class, calling `cmake` in
`DEB_SRCDIR` with classic parameters and then calling `make`. In the
call to `cmake`, the install prefix and path to CC/CXX as well as
CFLAGS/CXXFLAGS are given automatically. You can pass extra arguments
using `DEB_CMAKE_EXTRA_FLAGS`, and in strange cases where you need a
special configuration you can override the default arguments using
`DEB_CMAKE_NORMAL_ARGS`.

Using the qmake class
---------------------

This class is intended to handle build systems using
[qmake](http://doc.trolltech.com/4.5/qmake-manual.html), mostly used to
build Qt applications.

To use this class, add this line to your `debian/rules` : include
/usr/share/cdbs/1/class/qmake.mk

This class is an extension of the Makefile class, calling `qmake` in
`DEB_BUILDDIR` with classic parameters and then calling `make`. In the
call to `qmake`, the path to CC/CXX as well as
CPPFLAGS/CFLAGS/CPPFLAGS/CXXFLAGS are given automatically. Configuration
options can be given using `DEB_QMAKE_CONFIG_VAL` (resulting in adding
content to `CONFIG`), the later defaulting to `nostrip` if this option
is set in `DEB_BUILD_OPTIONS`.

Panthéon des exemples
=====================

Exemple de GNOME + autotools + patchsys simple
----------------------------------------------

(exemple du paquet « gnome-panel »)

`debian/control.in`: Source: gnome-panel Section: gnome Priority:
optional Maintainer: Marc Dequènes (Duck) \<Duck@DuckCorp.org\>
Uploaders: Sebastien Bacher \<seb128@debian.org\>, Arnaud Patard \\
\<arnaud.patard@rtp-net.org\>, @GNOME\_TEAM@ Standards-Version: 3.6.1.1
Build-Depends: @cdbs@, liborbit2-dev (\>= 2.10.2-1.1), intltool,
gnome-pkg-tools, \\ libglade2-dev (\>= 1:2.4.0), libwnck-dev (\>=
2.8.1-3), scrollkeeper \\ (\>= 0.3.14-9.1), libgnome-desktop-dev (\>=
2.8.3-2), libpng3-dev, sharutils, \\ libbonobo2-dev (\>= 2.8.0-3),
libxmu-dev, autotools-dev, libedata-cal-dev \\ (\>= 1.0.2-3) Package:
gnome-panel Architecture: any Depends: \${shlibs:Depends},
\${misc:Depends}, gnome-panel-data \\ (= \${Source-Version}),
gnome-desktop-data (\>= 2.8.1-2), gnome-session \\ (\>= 2.8.1-4),
gnome-control-center (\>= 1:2.8.1-3) Conflicts: gnome-panel2,
quick-lounge-applet (\<= 0.98-1), system-tray-applet, \\ metacity (\<=
2.6.0), menu (\<\< 2.1.9-1) Recommends: gnome-applets (\>= 2.8.2-1)
Suggests: menu (\>= 2.1.9-1), yelp, gnome2-user-guide, gnome-terminal |
\\ x-terminal-emulator, gnome-system-tools Description: launcher and
docking facility for GNOME 2 This package contains toolbar-like “panels”
which can be attached to the sides of your X desktop, or left
“floating”. It is designed to be used in conjunction with the Gnome
Desktop Environment. Many features are provided for use with the panels
– including an application menu, clock, mail checker, network monitor,
quick launch icons and the like. Package: libpanel-applet2-0 Section:
libs Architecture: any Depends: \${shlibs:Depends} Replaces: gnome-panel
(\<\< 2.6.0-2) Description: library for GNOME 2 panel applets This
library is used by GNOME 2 panel applets. Package: libpanel-applet2-dbg
Section: libdevel Architecture: any Depends: libpanel-applet2-0 (=
\${Source-Version}) Description: library for GNOME 2 panel applets -
library with debugging symbols This library is used by GNOME 2 panel
applets. . This package contains unstripped shared libraries. It is
provided primarily to provide a backtrace with names in a debugger, this
makes it somewhat easier to interpret core dumps. The libraries are
installed in /usr/lib/debug and can be used by placing that directory in
LD\_LIBRARY\_PATH. Most people will not need this package. Package:
libpanel-applet2-dev Section: libdevel Architecture: any Depends:
libpanel-applet2-0 (= \${Source-Version}), libgnomeui-dev (\>= 2.7.1-1)
Replaces: gnome-panel (\<\< 2.6.0-2), gnome-panel-data (\<\< 2.6.0)
Description: library for GNOME 2 panel applets - development files This
packages provides the include files and static library for the GNOME 2
panel applet library functions. Package: libpanel-applet2-doc Section:
doc Architecture: all Suggests: doc-base Replaces: libpanel-applet2-dev
(\<= 2.0.11-1) Description: library for GNOME 2 panel applets -
documentation files This packages provides the documentation files for
the GNOME 2 panel applet library functions. Package: gnome-panel-data
Section: gnome Architecture: all Depends: gnome-panel (=
\${Source-Version}), scrollkeeper (\>= 0.3.14-9.1), \\ \${misc:Depends}
Conflicts: gnome-panel-data2, gnome-core (\<\< 1.5) Replaces:
gnome-desktop-data (\<= 2.2.2-1), gnome-panel (\<\< 2.6.0-2)
Description: common files for GNOME 2 panel This package includes some
files that are needed by the GNOME 2 panel (Pixmaps, .desktop files and
internationalization files).

`debian/rules`: \#!/usr/bin/make -f \# Gnome Team include
/usr/share/gnome-pkg-tools/1/rules/uploaders.mk include
/usr/share/cdbs/1/rules/debhelper.mk \# L'inclusion ce fichier nous
donne un système de correctifs simple. Vous pouvez simplement \# déposer
les correctifs dans « debian/patches », et ils seront automatiquement \#
appliqués et désappliqués. include
/usr/share/cdbs/1/rules/simple-patchsys.mk \# Inclure ceci nous donne un
nombre de règles typique pour un programme \# GNOME, incluant la
définition de GCONF\_DISABLE\_MAKEFILE\_SCHEMA\_INSTALL=1. \# Notez que
cette classe hérite d'autotools.mk et de docbookxml.mk, \# donc vous
n'avez pas esoin de les inclure. include
/usr/share/cdbs/1/class/gnome.mk DEB\_CONFIGURE\_SCRIPT\_ENV :=
LDFLAGS="-Wl,-z,defs -Wl,-O1" DEB\_CONFIGURE\_EXTRA\_FLAGS :=
--enable-eds \# debug lib DEB\_DH\_STRIP\_ARGS :=
--dbg-package=libpanel-applet-2 \# tight versioning
DEB\_NOREVISION\_VERSION := \$(shell dpkg-parsechangelog | egrep
'\^Version:' | \\ cut -f 2 -d ' ' | cut -f 1 -d '-')
DEB\_DH\_MAKESHLIBS\_ARGS\_libpanel-applet2-0 := -V"libpanel-applet2-0
\\ (\>= \$(DEB\_NOREVISION\_VERSION))"
DEB\_SHLIBDEPS\_LIBRARY\_gnome-panel:= libpanel-applet2-0
DEB\_SHLIBDEPS\_INCLUDE\_gnome-panel :=
debian/libpanel-applet2-0/usr/lib/ binary-install/gnome-panel:: chmod
a+x debian/gnome-panel/usr/lib/gnome-panel/\*
binary-install/gnome-panel-data:: chmod a+x
debian/gnome-panel-data/etc/menu-methods/gnome-panel-data find
debian/gnome-panel-data/usr/share -type f -exec chmod -R a-x {} \\;
binary-install/libpanel-applet2-doc:: find
debian/libpanel-applet2-doc/usr/share/doc/libpanel-applet2-doc/ \\ -name
".arch-ids" -depth -exec rm -rf {} \\; clean:: \# L'équipe GNOME
« uploaders.mk » ne devrait pas outrepasser ce comportement \# voici un
contournement : sed -i "s/@cdbs@/\$(CDBS\_BUILD\_DEPENDS)/g"
debian/control \# cleanup not done by buildsys -find . -name "Makefile"
-exec rm -f {} \\;

Exemple Python
--------------

(exemple de « python-dice », un paquet non-officiel de DC)

`debian/control.in`: Source: python-dice Section: python Priority:
optional Maintainer: Marc Dequènes (Duck) \<Duck@DuckCorp.org\>
Standards-Version: 3.6.1.1 Build-Depends: @cdbs@, python2.3-dev,
python2.4-dev, swig, libdice2-dev \\ (\>= 0.6.2.fixed.1) Package:
python-dice Architecture: all Depends: python2.3-dice Description:
python bindings for dice rolling and simulation library PyDice is a
python module for dice rolling and simulation (using fuzzy logic). . It
provides a Python API to the libdice2 library. . This is a dummy package
automatically selecting the current Debian python version. Package:
python2.3-dice Architecture: any Depends: \${python:Depends}
Description: python bindings for dice rolling and simulation library
PyDice is a python module for dice rolling and simulation (using fuzzy
logic). . It provides a Python API to the libdice2 library. Package:
python2.4-dice Architecture: any Depends: \${python:Depends}
Description: python 2.4 bindings for dice rolling and simulation library
PyDice is a python module for dice rolling and simulation (using fuzzy
logic). . It provides a Python 2.4 API to the libdice2 library.

`debian/rules`: \#!/usr/bin/make -f include
/usr/share/cdbs/1/rules/debhelper.mk include
/usr/share/cdbs/1/class/python-distutils.mk

Exemple Makefile + Dpatch
-------------------------

(exemple du paquet « apg »)

`debian/control.in`: Source: apg Section: admin Priority: optional
Maintainer: Marc Haber \<mh+debian-packages@zugschlus.de\>
Build-Depends: @cdbs@ Standards-Version: 3.6.1 Package: apg
Architecture: any Depends: \${shlibs:Depends} Description: Automated
Password Generator - Standalone version APG (Automated Password
Generator) is the tool set for random password generation. It generates
some random words of required type and prints them to standard output.
This binary package contains only the standalone version of apg.
Advantages: \* Built-in ANSI X9.17 RNG (Random Number
Generator)(CAST/SHA1) \* Built-in password quality checking system (now
it has support for Bloom filter for faster access) \* Two Password
Generation Algorithms: 1. Pronounceable Password Generation Algorithm
(according to NIST FIPS 181) 2. Random Character Password Generation
Algorithm with 35 configurable modes of operation \* Configurable
password length parameters \* Configurable amount of generated passwords
\* Ability to initialize RNG with user string \* Support for /dev/random
\* Ability to crypt() generated passwords and print them as additional
output. \* Special parameters to use APG in script \* Ability to log
password generation requests for network version \* Ability to control
APG service access using tcpd \* Ability to use password generation
service from any type of box (Mac, WinXX, etc.) that connected to
network \* Ability to enforce remote users to use only allowed type of
password generation The client/server version of apg has been
deliberately omitted. . Upstream URL:
http://www.adel.nursat.kz/apg/download.shtml

`debian/rules`: \#!/usr/bin/make -f DEB\_MAKE\_CLEAN\_TARGET  := clean
DEB\_MAKE\_BUILD\_TARGET  := standalone DEB\_MAKE\_INSTALL\_TARGET  :=
install INSTALL\_PREFIX=\$(CURDIR)/debian/apg/usr include
/usr/share/cdbs/1/rules/debhelper.mk include
/usr/share/cdbs/1/rules/dpatch.mk include
/usr/share/cdbs/1/class/makefile.mk cleanbuilddir/apg:: rm -f
build-stamp configure-stamp php.tar.gz install/apg:: mv
\$(CURDIR)/debian/apg/usr/bin/apg \\
\$(CURDIR)/debian/apg/usr/lib/apg/apg tar --create --gzip --file
php.tar.gz --directory \\ \$(CURDIR)/php/apgonline/ . install -D
--mode=0644 php.tar.gz \\
\$(CURDIR)/debian/apg/usr/share/doc/apg/php.tar.gz rm php.tar.gz install
-D --mode=0755 \$(CURDIR)/debian/apg.wrapper \\
\$(CURDIR)/debian/apg/usr/bin/apg install -D --mode=0644
\$(CURDIR)/debian/apg.conf \\ \$(CURDIR)/debian/apg/etc/apg.conf

Exemple Perl
------------

(exemple du paquet « libmidi-perl »)

`debian/control`: Source: libmidi-perl Section: interpreters Priority:
optional Build-Depends: cdbs (\>= 0.4.4), debhelper (\>= 4.1.0), perl
(\>= 5.8.0-7) Maintainer: Mario Lang \<mlang@debian.org\>
Standards-Version: 3.5.10 Package: libmidi-perl Architecture: all
Depends: \${perl:Depends} Description: read, compose, modify, and write
MIDI files in Perl This suite of Perl modules provides routines for
reading, composing, modifying, and writing MIDI files.

`debian/rules`: \#!/usr/bin/make -f include
/usr/share/cdbs/1/rules/debhelper.mk include
/usr/share/cdbs/1/class/perlmodule.mk

Outils utiles
=============

`cdbs-edit-patch` (provided with CDBS)
--------------------------------------

This script is intended to help lazy people edit or easily create
patches for the simple-patchsys patch system.

Invoke this script with the name of the patch as argument, and you will
enter a copy of your work directory in a subshell where you can edit
sources. When your work is done and you are satisfied with your changes,
just exit the subshell and you will get back to normal world with
`debian/patches/.patch` created or modified accordingly. The script
takes care to apply previous patches (ordered patches needed !), current
patch if already existing (in case you want to update it), then generate
an incremental diff to only get desired modifications. If you want to
cancel the patch creation / modification, you only need to exit the
subshell with a non-zero value and the diff will not be generated (only
cleanups will be done).

Conclusion
==========

CDBS résoud la plupart des problème et est très plaisant à utiliser. De
plus en plus de DD l'utilisent, non pas parce qu'ils y sont obligé, mais
parce qu'ils l'ont gouté et ont trouvé qu'ils pouvaient améliorer leurs
paquets et éviter de perdre du temps à concevoir des règles idiotes et
complexes.

CDBS is not perfect, the BTS entry is not clear, but fixing a single bug
most of the time fix a problem for plenty of other packages. CDBS is not
yet capable of handling very complicated situations (like packages where
multiple C/C++ builds with different options and/or patches are
required), but this only affects a very small number of packages. These
limitations would be solved in CDBS2, which is work in progress (please
contact Jeff Bailey <jbailey@raspberryginger.com> if you want to help).

Utiliser CDBS plus largement améliorerait la qualité globale de Debian.
N'hésitez pas à l'essayer, à en parler à vos amis, et à contribuer.

Bon amusement avec CDBS !!! :-)

Merci à Jeff pour sa patience et avoir répondu à tant de questions.

Un merci spécial à GuiHome pour son aide pour relire cette
documentation.

This document is a [DocBook](http://docbook.org/) application, checked
using xmllint (from [libxml2](http://www.xmlsoft.org/)), produced using
xsltproc (from [libxslt](http://xmlsoft.org/XSLT/)), using the [N.
Walsh](http://nwalsh.com/) and
[dblatex](http://dblatex.sourceforge.net/) XLST stylesheets, and
converted with [LaTeX](http://www.latex-project.org/) tools (latex,
mkindex, pdflatex & dvips) /
[pstotext](http://research.compaq.com/SRC/virtualpaper/pstotext.html)
(with [GS](http://www.cs.wisc.edu/~ghost/)).
