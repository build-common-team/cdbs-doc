First steps
===========

Convert a package to CDBS
-------------------------

Converting to CDBS is easy; A simple `debian/rules` for a C/C++ software
with no extra rules would be written as this: 

    \#!/usr/bin/make -f
    include /usr/share/cdbs/1/rules/debhelper.mk
    include /usr/share/cdbs/1/class/autotools.mk

No, I'm not joking, this is sufficient to handle autotools management,
like updating `config.{guess|sub}`, cleanup temp files after build and
launch all common debhelper stuff.

Just use compat level 7 (or lower if needed, but not lower than 4 or it
may not work), create your `.install`, ``.info, etc as you usually do
with `dh_` commands, and CDBS would call them if necessary,
auto-detecting a lot of things.

> **Important**
>
> If `debian/control` management is activated (see below), build
> dependency on cdbs is automatically added, if not, you will have to do
> it yourself.

> **Warning**
>
> Beware your working directory *MUST NOT* have spaces or CDBS would
> probably fail; see [\#306941](http://bugs.debian.org/306941)
Basic settings and available variables
--------------------------------------

Most variables you can use in `debian/rules`:

  Variable                                                   Usage
  ---------------------------------------------------------- --------------------------------------------------------------------------------------------------------------------------------------------------------------
  **source package information**
  `DEB_SOURCE_PACKAGE`                                       name of the source package
  `DEB_VERSION`                                              full Debian version
  `DEB_NOEPOCH_VERSION`                                      Debian version without epoch
  `DEB_UPSTREAM_VERSION`                                     upstream version
  `DEB_ISNATIVE`                                             non-empty if package is native
  **binary packages information**
  `DEB_ALL_PACKAGES`                                         list of all binary packages
  `DEB_INDEP_PACKAGES`                                       list of architecture independent binary packages
  `DEB_ARCH_PACKAGES`                                        list of architecture dependant binary packages
  `DEB_UDEB_PACKAGES`                                        list of udeb binary packages, if any
  `DEB_PACKAGES`                                             list of non-udeb binary packages
  `DEB_INDEP_REGULAR_PACKAGES`                               list of non-udeb architecture independent binary packages
  `DEB_ARCH_REGULAR_PACKAGES`                                list of non-udeb architecture dependant binary packages
  `DEB_DBG_PACKAGES`                                         list of debug packages
  **system information**
  `DEB_HOST_GNU_TYPE`                                        GNU type on the host machine
  `DEB_HOST_GNU_SYSTEM`                                      system part of GNU type on the host machine
  `DEB_HOST_GNU_CPU`                                         CPU part of GNU type on the host machine
  `DEB_HOST_ARCH`                                            Debian architecture name on the host machine
  `DEB_HOST_ARCH_CPU`                                        CPU part of the Debian architecture name on the host machine
  `DEB_HOST_ARCH_OS`                                         OS part of the Debian architecture name on the host machine
  `DEB_BUILD_GNU_TYPE`                                       GNU type for this build
  `DEB_BUILD_GNU_SYSTEM`                                     system part of GNU type for this build
  `DEB_BUILD_GNU_CPU`                                        CPU part of GNU type for this build
  `DEB_BUILD_ARCH`                                           Debian architecture name for this build
  `DEB_BUILD_ARCH_CPU`                                       CPU part of the Debian architecture name for this build
  `DEB_BUILD_ARCH_OS`                                        OS part of the Debian architecture name for this build
  `cdbs_crossbuild`                                          non-empty if cross-building
  `DEB_ARCH`[^1]                                             old Debian architecture name
  **directories**
  `CURDIR`[^2]                                               package directory
  `DEB_SRCDIR`                                               where sources are (defaults to ".")
  `DEB_BUILDDIR`                                             in which directory to build (defaults to DEB\_SRCDIR)
  `DEB_DESTDIR`                                              in which directory to install the software (defaults to \$(CURDIR)/debian/\<package\> if there is only one binary package or \$(CURDIR)/debian/tmp/ if many)
  **per binary package, for 'action/package' targets[^3]**
  `cdbs_curpkg`                                              name of the package this target applies to
  **miscellaneous**
  `DEB_VERBOSE_ALL`                                          if set, ask CDBS to be more verbose

  : Variables commonly available in `debian/rules`

You can customize basic build parameters this way:

    DEB\_SRCDIR = \$(CURDIR)/src 
    DEB\_BUILDDIR = \$(DEB\_SRCDIR)/build
    DEB\_DESTDIR = \$(CURDIR)/plop/

Remember you can get the package directory using the `CURDIR` variable.

Custom build rules
------------------

CDBS defines extra make targets, and do needed actions in these rules.
You can use some of these targets as hooks to add your own
customizations. The CDBS classes sometimes add new targets for a
specific usage, and you can use some of them as well. Which targets are
usuable as hooks is easy: they use the make "double-colon rules"
feature, and most of them are described in this manual (we try to get in
sync with development to document them all).

> **Warning**
>
> Beware to use targets *after* needed CDBS includes.

Most targets you can use in `debian/rules` (called in this order):

  Target                                                                             Usage
  ---------------------------------------------------------------------------------- ------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **targets called during 'build'[^4] Debian essential target[^5], in this order**
  debian/control                                                                     actions after `debian/control` is generated, if you use `debian/control` management
  makebuilddir/package                                                               actions after build directory for package package has been created
  makebuilddir                                                                       actions after build directories for all packages have been created
  pre-build                                                                          actions before anything serious is done (like patching or configuring)
  update-config                                                                      actions after sources are patched and general build system files (like autotools scripts) are updated
  post-patches                                                                       actions after sources are ready to use
  common-configure-arch                                                              actions after configuration, for arch-dependent packages
  common-configure-indep                                                             actions after configuration, for arch-independent packages
  configure/package                                                                  actions after configuration, for the package package
  common-build-arch                                                                  actions after compilation, for arch-dependent packages
  common-build-indep                                                                 actions after compilation, for arch-independent packages
  build/package                                                                      actions after compilation, for the package package
  **targets called during 'binary' Debian essential target, in this order**
  common-post-build-arch                                                             actions immediately after build for the arch-dependent packages
  common-post-build-indep                                                            actions immediately after build for the arch-independent packages
  common-install-prehook-arch                                                        actions before anything serious is done (like installing files), for arch-dependent packages
  common-install-prehook-indep                                                       actions before anything serious is done (like installing files), for arch-independent packages
  common-install-arch                                                                actions after installation, for arch-dependent packages
  common-install-indep                                                               actions after installation, for arch-independent packages
  install/package                                                                    actions after installation, for the package package
  common-binary-arch                                                                 actions after everything is installed, for arch-dependent packages
  common-binary-indep                                                                actions after everything is installed, for arch-independent packages
  binary/package                                                                     actions after everything is installed, for the package package
  **targets called during 'clean' Debian essential target, in this order**
  cleanbuilddir/package                                                              actions after build directory for package package has been removed
  cleanbuilddir                                                                      actions after build directories for all packages have been removed
  reverse-config                                                                     actions after general build system files (like autotools scripts) are restored
  clean                                                                              actions after everything is cleaned (sources are unpatched and general build system file are removed, most[^6] files generated by the build process are removed)

  : Targets commonly available in `debian/rules`

The most useful for a majority of use cases are the action/package,
post-patches, and clean targets.

### Examples

Suppose you want custom rules for the source package foo, creating foo
(arch-dep) and foo-data (arch-indep), you simply need to add some lines
to `debian/rules`.

To add pre-configure actions: 

    makebuilddir/foo::
        ln -s plop plop2

To add post-configure actions: 

    configure/foo::
        sed -ri 's/PLOP/PLIP/' Makefile
        
    configure/foo-data::
        touch src/z.xml 
        
*/!\\ in this case we are talking about package configuration and NOT about
a configure script made with autotools.*

To add post-build actions:

    build/foo::
        /bin/bash debian/scripts/toto.sh
        
    build/foo-data::
        \$(MAKE) helpfiles

To add post-install actions:

    install/foo::
        cp debian/tmp/myfoocmd debian/foo/foocmd
        find debian/foo/ -name "CVS" -depth -exec rm -rf {} \\;
        
    install/foo-data::
        cp data/\*.png debian/foo-data/usr/share/foo-data/images/ 
        dh\_stuff -m ipot -f plop.bz3 debian/foo-data/libexec/
        

To add post deb preparation actions: 

    binary/foo::
        strip --remove-section=.comment --remove-section=.note --strip-unneeded \\
            debian/foo/usr/lib/foo/totoz.so

To add pre-clean actions: 

    cleanbuilddir/foo::
        rm -f debian/fooman.1

Common Build Options
--------------------

Default optimization is set using `DEB_OPT_FLAG` which default to "-O2";
you can override it. `CFLAGS` and `CXXFLAGS` are set to "-g -Wall
\$(DEB\_OPT\_FLAG)", CPPFLAGS is untouched from environment, but you can
override these settings on a per-package basis using `CFLAGS_`,
`CXXFLAGS_`, and `CPPFLAGS_` variables.

`DEB_BUILD_OPTIONS` is a well known Debian environment variable, not a
CDBS one, containing special build options (a comma-separated list of
keywords). CDBS does check `DEB_BUILD_OPTIONS` to take these options
into account; see details in each class.

`DEB_BUILD_PARALLEL`, if set, allow activating parallel builds in
certain classes (makefile, automake, perlmodule, qmake, cmake, and scons
classes use build commands supporting parallel builds). CDBS then uses
`DEB_PARALLEL_JOBS` to know how many builds to launch (which defaults to
`parallel=` parameter in `DEB_BUILD_OPTIONS`, but can be overridden).
