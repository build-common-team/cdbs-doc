Maintainer Tools
================

These are specials rules which can be activated by the Maintainer to do
extra checks or help automatize boring work, but having no direct link
with the software build itself.

Maintainer Mode
---------------

This mode aims at activating features which are not suitable for
automatic builds, but can help maintainers while they work on their
package. If `DEB_MAINTAINER_MODE` is set, this mode in activated and do
the following:

-   activate semi-automatic `debian/control` management
    (`DEB_AUTO_UPDATE_DEBIAN_CONTROL`)

-   activate a strict copyright check (if the utils rules are activated)

Semi-automatic `debian/control` management
------------------------------------------

> **Caution**
>
> Automatic `debian/control` generation using any tool is permitted into
> Debian as long as it is triggered manually by the developer and the
> latter checks the result carefully.
>
> Autogenerating `debian/control` without any human intervention could
> be harmful in some ways detailed in
> [\#311724](http://bugs.debian.org/311724). This is not allowed in
> Debian.
>
> We then urge you to avoid using `DEB_AUTO_UPDATE_DEBIAN_CONTROL`
> directly and instead invoke the autogeneration rules manually after
> you modified `debian/control.in` (this way users or buildds wouldn't
> have different Build-Depends when building, avoiding many problems).
> Do not forget to proofread the result before any upload.
>
> Manual `debian/control` regeneration:
> `DEB\_AUTO\_UPDATE\_DEBIAN\_CONTROL=yes fakeroot debian/rules clean`

This feature allow:

-   CDBS to automatically manage some build-related Build-Depends
    automatically

-   use of embedded shell commands

-   use of CPU and System criterias to specify architecture
    (*EXPERIMENTAL*)

Build-related Build-Depends are dependencies introduced by the use of
certain CDBS features, or auto-detected needs.

Embedded shell commands allows including hacks like: 

    Build-Depends: libgpm-dev [\`type-handling any linux-gnu\`]

CPU and System criterias implements support for Cpu/System fields, as a
replacement for the Architecture field (which is to be implemented in
dpkg in the long term, but still *EXPERIMENTAL*). Here is an example,
before:
    
     Architecture: all
     
and after:

    Cpu: all
    System: all
    
If these fields are used, it is also possible to include special tags to
easily take advantage of the type-handling tool, like in this example:

    Build-Depends: @cdbs@, procps [system: linux], plop [cpu: s390] 

(look at the type-handling package documentation, for more information)

You can also change the way build-dependencies are separated inside the
Build-Depends field using the `CDBS_BUILD_DEPENDS_DELIMITER` variable.
It defaults recommended multiline style (see Debian Policy §7.1), but
can be changed using this variable, or get back to the old default
(single line coma-separated list) if unset.

1.  Rename `debian/control` into `debian/control.in`.

2.  Replace cdbs / debhelper / ... Build-Depends with @cdbs@ in your
    `debian/control.in` like this:

        Build-Depends-Indep: @cdbs@, python-dev (>= 2.3), python-soya (>= 0.9), \
           python-soya (<< 0.10), python-openal(>= 0.1.4-4), gettext
                                

3.  Then manually (re)generate `debian/control` as explained above (see
    the caution part).

Package Check Utilities
-----------------------

To use these utilities, add this line to your `debian/rules`: 

    include /usr/share/cdbs/1/rules/utils.mk

### Copyright Check

This rule uses `licensecheck` (provided by devscripts) to check the
`debian/copyright` file.

You need to create the empty file `debian/copyright_hints` (with touch)
to activate this feature and get a report at the beginning of the build.
If you set `DEB_COPYRIGHT_CHECK_STRICT`, the build will fail if it could
not be run or problems were found by `licensecheck`.

You can customize the `licensecheck` parameters using
`DEB_COPYRIGHT_CHECK_ARGS`, and the files to check for copyright
information using `DEB_COPYRIGHT_CHECK_REGEX` and
`DEB_COPYRIGHT_CHECK_IGNORE_REGEX`.

### Build Infos

This rule uses `dh_buildinfo` (provided by dh-buildinfo), a tool to
track package versions used to build a package.

It will generate a `debian/buildinfo.gz` file, which will be
automatically installed in `/usr/share/doc/`.

### List Missing Files

This rule is intended to avoid missing new files in a new software
version, to allow the maintainer to check the list of not-installed
files. This is useful when multiple binary packages are created and
things may be lost in `debian/tmp`.

The rule must be called manually, *after a build with the `-nc`
`dpkg-buildpackage` option* (to avoid cleaning the `debian` directory):

    fakeroot debian/rules list-missing

It generates a list of installed files `debian/cdbs-install-list` found
in `debian/tmp` and a list of files installed in the package (or
ignored) `debian/cdbs-package-list`, then display the ones missing in
the package. You can add a list of files to ignored, because you want to
remember you wanted them excluded, in `debian/not-installed`. If you set
`DEB_BUILDINFO_STRICT`, the build will fail if it could not be
generated.

Upstream Source Management
--------------------------

This rule allows to fetch, repackage, possibly with files exclusion, the
upstream sources. You may also check the source tarball md5sum. It
provides a 'get-orig-source' target, as recommended by the Debian Policy
(chapter 4.9).

To use this rule, add these lines to your `debian/rules`: 

    include /usr/share/cdbs/1/rules/upstream-tarball.mk
    pkgbranch = 0.88 
    
    \# Optional: you may override the default tarball name (defaults to DEB\_SOURCE\_PACKAGE)
    DEB\_UPSTREAM\_PACKAGE = \$(DEB\_SOURCE\_PACKAGE:%-\$(pkgbranch)=%) 
    
    \# Compulsory
    DEB\_UPSTREAM\_URL = http://download.sugarlabs.org/sources/sucrose/glucose/\$(DEB\_UPSTREAM\_PACKAGE)
    DEB\_UPSTREAM\_TARBALL\_EXTENSION = tar.bz2 
    
    \# Optional: you can request a md5sum check
    DEB\_UPSTREAM\_TARBALL\_MD5 = f50a666c4e1f55b8fc7650258da0c539
    
    \# Optional: this list of space-separated files and directories will be removed when repackaging
    \# (Beware directories MUST end with a final '/' !!!)
    DEB\_UPSTREAM\_REPACKAGE\_EXCLUDES = .pc/

You can then fetch/repackage the source tarball using
`fakeroot debian/rules get-orig-source`. If you specified a non-empty
`DEB_UPSTREAM_REPACKAGE_EXCLUDES`, you can check if the current tarball
is properly up-to-date and do not contain newly excluded patterns using
`fakeroot debian/rules fail-source-not-repackaged`.
