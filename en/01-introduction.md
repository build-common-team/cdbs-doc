Introduction
============

A bit of history
----------------

CDBS was written by Jeff Bailey and Colin Walters in march 2003, later
joined by 4 other developers.

In 2004, we (Rtp and me) were experimenting CDBS, and it was quickly
obvious the lack of documentation (a very small set of examples and
spare notes shipped in the package) was preventing us from using it
widely in our packages. Thus, we started to write some notes on CDBS
usage, quickly growing to several pages. This documentation is a revised
and improved version from the original [DuckCorp Wiki
page](https://wiki.duckcorp.org/DebianPackagingTutorial_2fCDBS).

Nowadays, CDBS is mostly maintained by Peter Eisentraut and Jonas
Smedegaard (and occasionally me). Information on the project can be
found on the [alioth
page](http://alioth.debian.org/projects/build-common/); contributors
welcome ;-).

Why CDBS ?
----------

CDBS is designed to simplify the maintainer's work so that they only
need to think about packaging and not maintaining a `debian/rules` file
that keeps growing bigger and more complicated. So CDBS can handle for
you most of common rules and detect some parts of your configuration.

CDBS only uses simple makefile rules and is easily extensible using
classes. Classes for handling autotools buildsys, applying patches to
source, GNOME softwares, Python intall, and so on are available.

CDBS advantages:

-   short, readable and efficient `debian/rules`

-   automates debhelper and autotools for you so you don't have to
    bother about this unpleasant and repetitive tasks

-   maintainer can focus on real packaging problems because CDBS helps
    you but do not limit customization

-   classes used in CDBS have been well tested so you are using
    error-proof rules and avoid dirty hacks to solve common problems

-   switching to CDBS is easy

-   can be used to generate Debian files (like `debian/control` for
    GNOME Team Uploaders inclusion)

-   CDBS is easily extendable

-   It |70\>\< !!!
