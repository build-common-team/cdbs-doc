Common Tasks
============

Automatic tarball management
----------------------------

To use the CDBS tarball system, just add his line to your
`debian/rules`, and specify the name of the top directory of the
extracted tarball: 

    include /usr/share/cdbs/1/rules/tarball.mk
    DEB\_TAR\_SRCDIR := foosoft
 
CDBS will recognize tarballs with the following extensions: .tar .tgz
.tar.gz .tar.bz .tar.bz2 .tar.lzma .zip

The tarball location is auto-detected if in the top source directory, or
can be specified: 

    DEB\_TARBALL := \$(CURDIR)/tarballdir/mygnustuff\_beta-1.2.3.tar.gz

CDBS will handle automatic decompression and cleanups, automagically set
`DEB_SRCDIR` and `DEB_BUILDDIR` for you, and integrate well with other
CDBS parts (like autotools class). If `DEB_VERBOSE_ALL` is set, tarball
decompression is made verbose.

> **Warning**
>
> The `DEB_AUTO_CLEANUP_RCS` feature, used to clean up sources from
> dirty SCM-specific dirs and file , has been removed since version
> 0.4.39; use `DH_ALWAYS_EXCLUDE` (from debhelper) instead .

> **Important**
>
> If needed, and if `debian/control` management is activated (see
> below), build dependency on bzip2 or unzip is automatically added, if
> not, you will have to do it yourself.

Patching sources
----------------

### Rules for simple-patchsys

First, patching sources directly is really BAD™, so you need a way to
apply patches without touching any files. These rules, inspired by the
Dpatch system, are quite similar and powerful. All you need is
diff/patch knowledge, CDBS is doing the rest.

That's quite hard, so please listen carefully and prepare for
examination.

First, add this line to your `debian/rules`: 

    include /usr/share/cdbs/1/rules/simple-patchsys.mk
 
And then use it !

Create the directory `debian/patches` and put your patches in it. Files
should be named so as to reflect in which order they have to be applied,
and must finish with the `.patch` or `.diff` suffix. The class would
take care of patching before configure and unpatch after clean. It is
possible to use patch level 0 to 3, and CDBS would try them and use the
right level automatically. The system can handle compressed patch with
additional `.gz` or `.bz2` suffix and uu-encoded patches with additional
`.uu` suffix.

You can customize the directories where patches are searched, and the
suffix like this: (defaults are: .diff .diff.gz .diff.bz2 .diff.uu
.patch .patch.gz .patch.bz2 .patch.uu)

    DEB\_PATCHDIRS := debian/mypatches
    DEB\_PATCH\_SUFFIX := .plop

In case of errors when applying, for example
`debian/patches/01_hurd_ftbfs_pathmax.patch`, you can read the log for
this patch in `debian/patches/01_hurd_ftbfs_pathmax.patch.level-0.log`
('0' because a level 0 patch).

> **Important**
>
> If `debian/control` management is activated (see below), build
> dependency on patchutils is automatically added, if not, you will have
> to do it yourself.

### Rules for dpatch

To use Dpatch as an alternative to the CDBS included patch system, just
add his line to your `debian/rules`: 

    include /usr/share/cdbs/1/rules/dpatch.mk 
    \# needed to use the dpatch tools like dpatch-edit-patch)
    include /usr/share/dpatch/dpatch.make
 
Now you can use Dpatch as usual and CDBS would call it automatically.

> **Warning**
>
> You should include dpatch.mk *AFTER* autotools.mk or gnome.mk in order
> to have dpatch extension work correctly.

> **Important**
>
> If `debian/control` management is activated (see below), build
> dependency on dpatch and patchutils is automatically added, if not,
> you will have to do it yourself.

### Rules for quilt

To use [Quilt](http://savannah.nongnu.org/projects/quilt) as an
alternative to the CDBS included patch system, just add his line to your
`debian/rules`:

    include /usr/share/cdbs/1/rules/patchsys-quilt.mk
 
Now you can use Quilt as usual and CDBS would call it automatically.

> **Important**
>
> If `debian/control` management is activated (see below), build
> dependency on quilt and patchutils is automatically added, if not, you
> will have to do it yourself.

Using Debhelper
---------------

### Not managing Debhelper

Yes, CDBS is doing almost everything for you :).

Just add this line to the beginning of your `debian/rules` file:

    include /usr/share/cdbs/1/rules/debhelper.mk

In case of a missing compat information, CDBS would create a
`debian/compat` file with compatibility level 7. If you are using an
obsolete `DH_COMPAT` variable in your `debian/rules`, you should get rid
of it. In this case, or in case you would like CDBS not to create a
`debian/compat` file, you can disable this feature by setting
`DEB_DH_COMPAT_DISABLE` to a non-void value.

Extra variables you can use in `debian/rules` with these rules:

  Variable                                               Usage
  ------------------------------------------------------ --------------------------------------------------------------------------
  **debhelper**
  `DH_COMPAT`                                            reflect the debhelper compatibility level (set by CDBS if unset)
  `DH_VERBOSE`                                           ask debhelper to be verbose (defaults to the value of `DEB_VERBOSE_ALL`)
  **per binary package, for 'action/package' targets**
  `is_debug_package`                                     set if the package is a debug package

  : Variables available in `debian/rules` with debhelper rules

Extra targets can be used in `debian/rules` with these rules (called in
this order):

  Target                                                                                                                          Usage
  ------------------------------------------------------------------------------------------------------------------------------- ----------------------------------------------------------------------------------------------------------------------------
  **targets called just before 'common-install-prehook-arch' and 'common-install-prehook-indep' common targets, in this order**
  common-install-prehook-impl                                                                                                     actions before installation and after debhelper prepared the directories
  **targets called just before 'build/package' common targets, in this order**
  binary-install/package                                                                                                          actions after the upstream build system and debhelper installed everything
  binary-post-install/package                                                                                                     actions after everything is installed (after the 'binary-install' rule, which may be customized in a class or by yourself)
  binary-strip/package                                                                                                            actions after stripping executables and libraries
  binary-fixup/package                                                                                                            actions after gzipping certain files (doc, examples, ...) and fixing permissions
  binary-predeb/package                                                                                                           actions just before creating the debs
  binary-makedeb/package                                                                                                          actions just after creating the debs

  : Targets available in `debian/rules` with debhelper rules

For example, to add post install preparation (after debhelper work):

    binary-install/foo::
        chmod a+x debian/foo/usr/bin/pouet

These rules handles the following debhelper commands for each binary
package automatically (with the parameters you can use to customize
their behavior):

  Commands                                                                      Parameters
  ----------------------------------------------------------------------------- ------------------------------------------------------------------------------------------------------------------- ---------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **commands called during 'common-install-prehook-impl rule, in this order**
  `dh_clean`[^7]                                                                `DEB_CLEAN_EXCLUDE`                                                                                                 regular expressions matching files which should not be cleaned
  `dh_prep`[^8]                                                                 `DEB_CLEAN_EXCLUDE`                                                                                                 regular expressions matching files which should not be cleaned
  `dh_installdirs`                                                              `DEB_INSTALL_DIRS_ALL`                                                                                              subdirectories to create in installation staging directory, for all packages
  **commands called during 'install/package' rules, in this order**
  `dh_installdirs`                                                              `DEB_INSTALL_DIRS_`                                                                                                 subdirectories to create in installation staging directory, for package package
  **commands called during 'binary-install/package' rules, in this order**
  `dh_installdocs`                                                              `DEB_INSTALL_DOCS_ALL`                                                                                              files which should go in `/usr/share/doc/`, for all packages
  `DEB_INSTALL_DOCS_`                                                           files which should go in `/usr/share/doc/`, for package package
  `dh_installexamples`                                                          `DEB_INSTALL_EXAMPLES_`                                                                                             files which should go in `/usr/share/doc//examples`, for package package
  `dh_installman`                                                               `DEB_INSTALL_MANPAGES_`                                                                                             manpages to install in the package, for package package
  `dh_installinfo`                                                              `DEB_INSTALL_INFO_`                                                                                                 info files which should go in `/usr/share/info`, for package package
  `dh_installmenu`                                                              `DEB_DH_INSTALL_MENU_ARGS`                                                                                          extra arguments passed to the command
  `dh_installcron`                                                              `DEB_DH_INSTALL_CRON_ARGS`                                                                                          extra arguments passed to the command
  `dh_installinit`                                                              `DEB_UPDATE_RCD_PARAMS_`                                                                                            arguments passed to update-rc.d in init scripts, for package package
  `DEB_UPDATE_RCD_PARAMS`                                                       arguments passed to update-rc.d in init scripts, for all packages (overrides `DEB_UPDATE_RCD_PARAMS_` parameters)
  `DEB_DH_INSTALLINIT_ARGS`                                                     extra arguments passed to the command
  `dh_installdebconf`                                                           `DEB_DH_INSTALLDEBCONF_ARGS`                                                                                        extra arguments passed to the command
  `dh_installemacsen`                                                           `DEB_EMACS_PRIORITY`                                                                                                overrides the default priority for the site-start.d file
  `DEB_EMACS_FLAVOR`                                                            overrides the default Emacs flavor for the site-start.d file
  `DEB_DH_INSTALLEMACSEN_ARGS`                                                  extra arguments passed to the command
  `dh_installcatalogs`                                                          `DEB_DH_INSTALLCATALOGS_ARGS`                                                                                       extra arguments passed to the command
  `dh_installpam`                                                               `DEB_DH_INSTALLPAM_ARGS`                                                                                            extra arguments passed to the command
  `dh_installlogrotate`                                                         `DEB_DH_INSTALLLOGROTATE_ARGS`                                                                                      extra arguments passed to the command
  `dh_installlogcheck`                                                          `DEB_DH_INSTALLLOGCHECK_ARGS`                                                                                       extra arguments passed to the command
  `dh_installchangelogs`                                                        `DEB_INSTALL_CHANGELOGS_ALL`                                                                                        files which should be interpreted as upstream changelog, for all packages
  `DEB_INSTALL_CHANGELOGS_`                                                     files which should be interpreted as upstream changelog, for package package
  `DEB_DH_INSTALLCHANGELOGS_ARGS`                                               extra arguments passed to the command
  `dh_installudev`                                                              `DEB_DH_INSTALLUDEV_ARGS`                                                                                           extra arguments passed to the command
  `dh_lintian`                                                                  `DEB_DH_LINTIAN_ARGS`                                                                                               extra arguments passed to the command
  `dh_install`                                                                  `DEB_DH_INSTALL_SOURCEDIR`[^9]                                                                                      directory where to look for files to install
  `DEB_DH_INSTALL_ARGS`                                                         extra arguments passed to the command
  `dh_link`                                                                     `DEB_DH_LINK_ARGS`                                                                                                  extra arguments passed to the command, for all packages
  `DEB_DH_LINK_`                                                                extra arguments passed to the command, for package package
  `dh_installmime`                                                              `DEB_DH_INSTALLMIME_ARGS`                                                                                           extra arguments passed to the command
  **commands called during 'binary-strip/package' rules, in this order**
  `dh_strip`[^10]                                                               `DEB_STRIP_EXCLUDE`                                                                                                 regular expressions matching files which should not be stripped
  `DEB_DH_STRIP_ARGS`                                                           extra arguments passed to the command
  **commands called during 'binary-fixup/package' rules, in this order**
  `dh_compress`                                                                 `DEB_COMPRESS_EXCLUDE`                                                                                              regular expressions matching files which should not be compressed
  `dh_fixperms`                                                                 `DEB_FIXPERMS_EXCLUDE`                                                                                              regular expressions matching files which should not have their permissions changed
  `DEB_DH_FIXPERMS_ARGS`                                                        extra arguments passed to the command
  `dh_makeshlibs`                                                               `DEB_DH_MAKESHLIBS_ARGS_ALL`                                                                                        extra arguments passed the command, for all packages
  `DEB_DH_MAKESHLIBS_ARGS_`                                                     extra arguments passed to the command, for package package
  `DEB_DH_MAKESHLIBS_ARGS`                                                      completely override argument passed to the command
  **commands called during 'binary-predeb/package' rules, in this order**
  `dh_installdeb`                                                               `DEB_DH_INSTALLDEB_ARGS`                                                                                            extra arguments passed to the command
  `dh_perl`                                                                     `DEB_PERL_INCLUDE`                                                                                                  space-separated list of paths to search for perl modules, for all packages
  `DEB_PERL_INCLUDE_`                                                           space-separated list of paths to search for perl modules, for package package
  `DEB_DH_PERL_ARGS`                                                            completely override argument passed to the command
  `dh_shlibdeps`                                                                `DEB_SHLIBDEPS_LIBRARY_`[^11]                                                                                       if set, look first in the package build directory for the specified package name, when searching for libraries, symbol files, and shlibs files, for package package
  `DEB_SHLIBDEPS_INCLUDE`[^12]                                                  space-separated list of paths to search first for dependency info, for package package
  `DEB_SHLIBDEPS_INCLUDE_$`                                                     space-separated list of paths to search first for dependency info, for package package
  `DEB_DH_SHLIBDEPS_ARGS_ALL`                                                   extra arguments passed the command, for all packages
  `DEB_DH_SHLIBDEPS_ARGS_`                                                      extra arguments passed to the command, for package package
  `DEB_DH_SHLIBDEPS_ARGS`                                                       completely override argument passed to the command
  **commands called during 'binary-makedeb/package' rules, in this order**
  `dh_gencontrol`                                                               `DEB_DH_GENCONTROL_ARGS_ALL`                                                                                        extra arguments passed the command, for all packages
  `DEB_DH_GENCONTROL_ARGS_`                                                     extra arguments passed to the command, for package package
  `DEB_DH_GENCONTROL_ARGS`                                                      completely override argument passed to the command
  `dh_md5sums`                                                                  `DEB_DH_MD5SUMS_ARGS`                                                                                               extra arguments passed the command
  `dh_builddeb`                                                                 `DEB_DH_BUILDDEB_ENV`                                                                                               environment passed to the command (pass DH\_ALWAYS\_EXCLUDE by default)
  `DEB_DH_BUILDDEB_ARGS`                                                        extra arguments passed to the command
  **commands called during 'clean' rule, in this order**
  `dh_clean`                                                                    `DEB_CLEAN_EXCLUDE`                                                                                                 regular expressions matching files which should not be cleaned

  : Debhelper commands commonly managed

Other debhelper commands can be handled in specific classes or may be
called in custom rules.

> **Important**
>
> If `debian/control` management is activated (see below), build
> dependency on debhelper is automatically added, if not, you will have
> to do it yourself.
>
> Having a versioned dependency on debhelper is recommended as it will
> ensure people will use the version providing the necessary features
> (CDBS `debian/control` management will do it).

### Debhelper customization examples

To specify a tight dependency on a package containing shared libraries:

    DEB\_DH\_MAKESHLIBS\_ARGS\_libfoo := -V"libfoo (\>= 0.1.2-3)"
    DEB\_SHLIBDEPS\_LIBRARY\_arkrpg := libfoo
    DEB\_SHLIBDEPS\_INCLUDE\_arkrpg := debian/libfoo/usr/lib/

To install a changelog file with an uncommon name as
`ProjectChanges.txt.gz`:

    DEB\_INSTALL\_CHANGELOGS\_ALL := ProjectChanges.txt

To avoid compressing files with i`.py` extension: 

    DEB\_COMPRESS\_EXCLUDE := .py

To register a debug library package libfoo-dbg for libfoo (which needs
unstripped `.so`) in compat mode 4:

    DEB\_DH\_STRIP\_ARGS := --dbg-package=libfoo
    
Starting from compat mode 5, CDBS automatically detect -dbg packages and
pass the needed arguments to `dh_strip`; `DEB_DH_STRIP_ARGS` can still be
useful to pass additional parameters like excluded items (`--exclude=`).

Perl-specific debhelper options (`dh_perl` call is always performed): 

    \# Add a space-separated list of paths to search for perl modules
    DEB\_PERL\_INCLUDE := /usr/lib/perl-z 
    \# Like the above, but for the 'libperl-stuff' package
    DEB\_PERL\_INCLUDE\_libperl-stuff := /usr/lib/perl-plop
    \# Overrides options passed to dh\_perl
    DEB\_DH\_PERL\_ARGS := -d

To avoid loosing temporary generated files in `dh_clean` processing
(rarely useful):
    
    \# files containing these pattern would not be deleted
    \# (beware CDBS changelog has a typo while highlighting new
    DEB\_CLEAN\_EXCLUDE\*S\* feature) DEB\_CLEAN\_EXCLUDE := precious keep

Cross-Building
--------------

Since version 0.4.83, CDBS has a working cross-building support. If
cross-building, a few adaptations are automatically made:

-   the `cdbs_crossbuild` variable is automatically set (and can be used
    in `debian/rules`)

-   in the makefile class: CC is passed to the make calls with the value
    "\$(`DEB_HOST_GNU_TYPE`)-gcc"

-   in the autotools class: CC and CXX are only passed to the configure
    call if explicitly set in environment or `debian/rules`
