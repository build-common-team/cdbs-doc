Foreword
========

This documentation describes what we succeeded to learn about CDBS
usage, with as much details as possible. Nevertheless, we are not using
the whole set of available features ourselves, and some parts of this
documentation were written for mere convenience and completeness.

Please note some examples in this documentation contain folded content
which is necessary to keep the pages at a reasonable width; take care to
unfold them when necessary before using them (e.g., parts of the
`debian/control` content must not be folded or build will fail or result
be incorrect).

If you find mistakes or missing information, feel free to contact Marc
Dequènes (Duck) <duck@duckcorp.org>.

Introduction
============

A bit of history
----------------

CDBS was written by Jeff Bailey and Colin Walters in march 2003, later
joined by 4 other developers.

In 2004, we (Rtp and me) were experimenting CDBS, and it was quickly
obvious the lack of documentation (a very small set of examples and
spare notes shipped in the package) was preventing us from using it
widely in our packages. Thus, we started to write some notes on CDBS
usage, quickly growing to several pages. This documentation is a revised
and improved version from the original [DuckCorp Wiki
page](https://wiki.duckcorp.org/DebianPackagingTutorial_2fCDBS).

Nowadays, CDBS is mostly maintained by Peter Eisentraut and Jonas
Smedegaard (and occasionally me). Information on the project can be
found on the [alioth
page](http://alioth.debian.org/projects/build-common/); contributors
welcome ;-).

Why CDBS ?
----------

CDBS is designed to simplify the maintainer's work so that they only
need to think about packaging and not maintaining a `debian/rules` file
that keeps growing bigger and more complicated. So CDBS can handle for
you most of common rules and detect some parts of your configuration.

CDBS only uses simple makefile rules and is easily extensible using
classes. Classes for handling autotools buildsys, applying patches to
source, GNOME softwares, Python intall, and so on are available.

CDBS advantages:

-   short, readable and efficient `debian/rules`

-   automates debhelper and autotools for you so you don't have to
    bother about this unpleasant and repetitive tasks

-   maintainer can focus on real packaging problems because CDBS helps
    you but do not limit customization

-   classes used in CDBS have been well tested so you are using
    error-proof rules and avoid dirty hacks to solve common problems

-   switching to CDBS is easy

-   can be used to generate Debian files (like `debian/control` for
    GNOME Team Uploaders inclusion)

-   CDBS is easily extendable

-   It |70\>\< !!!

First steps
===========

Convert a package to CDBS
-------------------------

Converting to CDBS is easy; A simple `debian/rules` for a C/C++ software
with no extra rules would be written as this: \#!/usr/bin/make -f
include /usr/share/cdbs/1/rules/debhelper.mk include
/usr/share/cdbs/1/class/autotools.mk

No, I'm not joking, this is sufficient to handle autotools management,
like updating `config.{guess|sub}`, cleanup temp files after build and
launch all common debhelper stuff.

Just use compat level 7 (or lower if needed, but not lower than 4 or it
may not work), create your `.install`, ``.info, etc as you usually do
with `dh_` commands, and CDBS would call them if necessary,
auto-detecting a lot of things.

> **Important**
>
> If `debian/control` management is activated (see below), build
> dependency on cdbs is automatically added, if not, you will have to do
> it yourself.

> **Warning**
>
> Beware your working directory *MUST NOT* have spaces or CDBS would
> probably fail; see [\#306941](http://bugs.debian.org/306941)

Basic settings and available variables
--------------------------------------

Most variables you can use in `debian/rules`:

  Variable                                                   Usage
  ---------------------------------------------------------- --------------------------------------------------------------------------------------------------------------------------------------------------------------
  **source package information**
  `DEB_SOURCE_PACKAGE`                                       name of the source package
  `DEB_VERSION`                                              full Debian version
  `DEB_NOEPOCH_VERSION`                                      Debian version without epoch
  `DEB_UPSTREAM_VERSION`                                     upstream version
  `DEB_ISNATIVE`                                             non-empty if package is native
  **binary packages information**
  `DEB_ALL_PACKAGES`                                         list of all binary packages
  `DEB_INDEP_PACKAGES`                                       list of architecture independent binary packages
  `DEB_ARCH_PACKAGES`                                        list of architecture dependant binary packages
  `DEB_UDEB_PACKAGES`                                        list of udeb binary packages, if any
  `DEB_PACKAGES`                                             list of non-udeb binary packages
  `DEB_INDEP_REGULAR_PACKAGES`                               list of non-udeb architecture independent binary packages
  `DEB_ARCH_REGULAR_PACKAGES`                                list of non-udeb architecture dependant binary packages
  `DEB_DBG_PACKAGES`                                         list of debug packages
  **system information**
  `DEB_HOST_GNU_TYPE`                                        GNU type on the host machine
  `DEB_HOST_GNU_SYSTEM`                                      system part of GNU type on the host machine
  `DEB_HOST_GNU_CPU`                                         CPU part of GNU type on the host machine
  `DEB_HOST_ARCH`                                            Debian architecture name on the host machine
  `DEB_HOST_ARCH_CPU`                                        CPU part of the Debian architecture name on the host machine
  `DEB_HOST_ARCH_OS`                                         OS part of the Debian architecture name on the host machine
  `DEB_BUILD_GNU_TYPE`                                       GNU type for this build
  `DEB_BUILD_GNU_SYSTEM`                                     system part of GNU type for this build
  `DEB_BUILD_GNU_CPU`                                        CPU part of GNU type for this build
  `DEB_BUILD_ARCH`                                           Debian architecture name for this build
  `DEB_BUILD_ARCH_CPU`                                       CPU part of the Debian architecture name for this build
  `DEB_BUILD_ARCH_OS`                                        OS part of the Debian architecture name for this build
  `cdbs_crossbuild`                                          non-empty if cross-building
  `DEB_ARCH`[^1]                                             old Debian architecture name
  **directories**
  `CURDIR`[^2]                                               package directory
  `DEB_SRCDIR`                                               where sources are (defaults to ".")
  `DEB_BUILDDIR`                                             in which directory to build (defaults to DEB\_SRCDIR)
  `DEB_DESTDIR`                                              in which directory to install the software (defaults to \$(CURDIR)/debian/\<package\> if there is only one binary package or \$(CURDIR)/debian/tmp/ if many)
  **per binary package, for 'action/package' targets[^3]**
  `cdbs_curpkg`                                              name of the package this target applies to
  **miscellaneous**
  `DEB_VERBOSE_ALL`                                          if set, ask CDBS to be more verbose

  : Variables commonly available in `debian/rules`

You can customize basic build parameters this way: DEB\_SRCDIR =
\$(CURDIR)/src DEB\_BUILDDIR = \$(DEB\_SRCDIR)/build DEB\_DESTDIR =
\$(CURDIR)/plop/

Remember you can get the package directory using the `CURDIR` variable.

Custom build rules
------------------

CDBS defines extra make targets, and do needed actions in these rules.
You can use some of these targets as hooks to add your own
customizations. The CDBS classes sometimes add new targets for a
specific usage, and you can use some of them as well. Which targets are
usuable as hooks is easy: they use the make "double-colon rules"
feature, and most of them are described in this manual (we try to get in
sync with development to document them all).

> **Warning**
>
> Beware to use targets *after* needed CDBS includes.

Most targets you can use in `debian/rules` (called in this order):

  Target                                                                             Usage
  ---------------------------------------------------------------------------------- ------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **targets called during 'build'[^4] Debian essential target[^5], in this order**
  debian/control                                                                     actions after `debian/control` is generated, if you use `debian/control` management
  makebuilddir/package                                                               actions after build directory for package package has been created
  makebuilddir                                                                       actions after build directories for all packages have been created
  pre-build                                                                          actions before anything serious is done (like patching or configuring)
  update-config                                                                      actions after sources are patched and general build system files (like autotools scripts) are updated
  post-patches                                                                       actions after sources are ready to use
  common-configure-arch                                                              actions after configuration, for arch-dependent packages
  common-configure-indep                                                             actions after configuration, for arch-independent packages
  configure/package                                                                  actions after configuration, for the package package
  common-build-arch                                                                  actions after compilation, for arch-dependent packages
  common-build-indep                                                                 actions after compilation, for arch-independent packages
  build/package                                                                      actions after compilation, for the package package
  **targets called during 'binary' Debian essential target, in this order**
  common-post-build-arch                                                             actions immediately after build for the arch-dependent packages
  common-post-build-indep                                                            actions immediately after build for the arch-independent packages
  common-install-prehook-arch                                                        actions before anything serious is done (like installing files), for arch-dependent packages
  common-install-prehook-indep                                                       actions before anything serious is done (like installing files), for arch-independent packages
  common-install-arch                                                                actions after installation, for arch-dependent packages
  common-install-indep                                                               actions after installation, for arch-independent packages
  install/package                                                                    actions after installation, for the package package
  common-binary-arch                                                                 actions after everything is installed, for arch-dependent packages
  common-binary-indep                                                                actions after everything is installed, for arch-independent packages
  binary/package                                                                     actions after everything is installed, for the package package
  **targets called during 'clean' Debian essential target, in this order**
  cleanbuilddir/package                                                              actions after build directory for package package has been removed
  cleanbuilddir                                                                      actions after build directories for all packages have been removed
  reverse-config                                                                     actions after general build system files (like autotools scripts) are restored
  clean                                                                              actions after everything is cleaned (sources are unpatched and general build system file are removed, most[^6] files generated by the build process are removed)

  : Targets commonly available in `debian/rules`

The most useful for a majority of use cases are the action/package,
post-patches, and clean targets.

### Examples

Suppose you want custom rules for the source package foo, creating foo
(arch-dep) and foo-data (arch-indep), you simply need to add some lines
to `debian/rules`.

To add pre-configure actions: makebuilddir/foo:: ln -s plop plop2

To add post-configure actions: configure/foo:: sed -ri 's/PLOP/PLIP/'
Makefile configure/foo-data:: touch src/z.xml */!\\ in this case we are
talking about package configuration and NOT about a configure script
made with autotools.*

To add post-build actions: build/foo:: /bin/bash debian/scripts/toto.sh
build/foo-data:: \$(MAKE) helpfiles

To add post-install actions: install/foo:: cp debian/tmp/myfoocmd
debian/foo/foocmd find debian/foo/ -name "CVS" -depth -exec rm -rf {}
\\; install/foo-data:: cp data/\*.png
debian/foo-data/usr/share/foo-data/images/ dh\_stuff -m ipot -f plop.bz3
debian/foo-data/libexec/

To add post deb preparation actions: binary/foo:: strip
--remove-section=.comment --remove-section=.note --strip-unneeded \\
debian/foo/usr/lib/foo/totoz.so

To add pre-clean actions: cleanbuilddir/foo:: rm -f debian/fooman.1

Common Build Options
--------------------

Default optimization is set using `DEB_OPT_FLAG` which default to "-O2";
you can override it. `CFLAGS` and `CXXFLAGS` are set to "-g -Wall
\$(DEB\_OPT\_FLAG)", CPPFLAGS is untouched from environment, but you can
override these settings on a per-package basis using `CFLAGS_`,
`CXXFLAGS_`, and `CPPFLAGS_` variables.

`DEB_BUILD_OPTIONS` is a well known Debian environment variable, not a
CDBS one, containing special build options (a comma-separated list of
keywords). CDBS does check `DEB_BUILD_OPTIONS` to take these options
into account; see details in each class.

`DEB_BUILD_PARALLEL`, if set, allow activating parallel builds in
certain classes (makefile, automake, perlmodule, qmake, cmake, and scons
classes use build commands supporting parallel builds). CDBS then uses
`DEB_PARALLEL_JOBS` to know how many builds to launch (which defaults to
`parallel=` parameter in `DEB_BUILD_OPTIONS`, but can be overridden).

Common Tasks
============

Automatic tarball management
----------------------------

To use the CDBS tarball system, just add his line to your
`debian/rules`, and specify the name of the top directory of the
extracted tarball: include /usr/share/cdbs/1/rules/tarball.mk
DEB\_TAR\_SRCDIR := foosoft CDBS will recognize tarballs with the
following extensions: .tar .tgz .tar.gz .tar.bz .tar.bz2 .tar.lzma .zip

The tarball location is auto-detected if in the top source directory, or
can be specified: DEB\_TARBALL :=
\$(CURDIR)/tarballdir/mygnustuff\_beta-1.2.3.tar.gz

CDBS will handle automatic decompression and cleanups, automagically set
`DEB_SRCDIR` and `DEB_BUILDDIR` for you, and integrate well with other
CDBS parts (like autotools class). If `DEB_VERBOSE_ALL` is set, tarball
decompression is made verbose.

> **Warning**
>
> The `DEB_AUTO_CLEANUP_RCS` feature, used to clean up sources from
> dirty SCM-specific dirs and file , has been removed since version
> 0.4.39; use `DH_ALWAYS_EXCLUDE` (from debhelper) instead .

> **Important**
>
> If needed, and if `debian/control` management is activated (see
> below), build dependency on bzip2 or unzip is automatically added, if
> not, you will have to do it yourself.

Patching sources
----------------

### Rules for simple-patchsys

First, patching sources directly is really BAD™, so you need a way to
apply patches without touching any files. These rules, inspired by the
Dpatch system, are quite similar and powerful. All you need is
diff/patch knowledge, CDBS is doing the rest.

That's quite hard, so please listen carefully and prepare for
examination.

First, add this line to your `debian/rules`: include
/usr/share/cdbs/1/rules/simple-patchsys.mk And then use it !

Create the directory `debian/patches` and put your patches in it. Files
should be named so as to reflect in which order they have to be applied,
and must finish with the `.patch` or `.diff` suffix. The class would
take care of patching before configure and unpatch after clean. It is
possible to use patch level 0 to 3, and CDBS would try them and use the
right level automatically. The system can handle compressed patch with
additional `.gz` or `.bz2` suffix and uu-encoded patches with additional
`.uu` suffix.

You can customize the directories where patches are searched, and the
suffix like this: (defaults are: .diff .diff.gz .diff.bz2 .diff.uu
.patch .patch.gz .patch.bz2 .patch.uu) DEB\_PATCHDIRS :=
debian/mypatches DEB\_PATCH\_SUFFIX := .plop

In case of errors when applying, for example
`debian/patches/01_hurd_ftbfs_pathmax.patch`, you can read the log for
this patch in `debian/patches/01_hurd_ftbfs_pathmax.patch.level-0.log`
('0' because a level 0 patch).

> **Important**
>
> If `debian/control` management is activated (see below), build
> dependency on patchutils is automatically added, if not, you will have
> to do it yourself.

### Rules for dpatch

To use Dpatch as an alternative to the CDBS included patch system, just
add his line to your `debian/rules`: include
/usr/share/cdbs/1/rules/dpatch.mk \# needed to use the dpatch tools
(like dpatch-edit-patch) include /usr/share/dpatch/dpatch.make Now you
can use Dpatch as usual and CDBS would call it automatically.

> **Warning**
>
> You should include dpatch.mk *AFTER* autotools.mk or gnome.mk in order
> to have dpatch extension work correctly.

> **Important**
>
> If `debian/control` management is activated (see below), build
> dependency on dpatch and patchutils is automatically added, if not,
> you will have to do it yourself.

### Rules for quilt

To use [Quilt](http://savannah.nongnu.org/projects/quilt) as an
alternative to the CDBS included patch system, just add his line to your
`debian/rules`: include /usr/share/cdbs/1/rules/patchsys-quilt.mk Now
you can use Quilt as usual and CDBS would call it automatically.

> **Important**
>
> If `debian/control` management is activated (see below), build
> dependency on quilt and patchutils is automatically added, if not, you
> will have to do it yourself.

Using Debhelper
---------------

### Not managing Debhelper

Yes, CDBS is doing almost everything for you :).

Just add this line to the beginning of your `debian/rules` file: include
/usr/share/cdbs/1/rules/debhelper.mk

In case of a missing compat information, CDBS would create a
`debian/compat` file with compatibility level 7. If you are using an
obsolete `DH_COMPAT` variable in your `debian/rules`, you should get rid
of it. In this case, or in case you would like CDBS not to create a
`debian/compat` file, you can disable this feature by setting
`DEB_DH_COMPAT_DISABLE` to a non-void value.

Extra variables you can use in `debian/rules` with these rules:

  Variable                                               Usage
  ------------------------------------------------------ --------------------------------------------------------------------------
  **debhelper**
  `DH_COMPAT`                                            reflect the debhelper compatibility level (set by CDBS if unset)
  `DH_VERBOSE`                                           ask debhelper to be verbose (defaults to the value of `DEB_VERBOSE_ALL`)
  **per binary package, for 'action/package' targets**
  `is_debug_package`                                     set if the package is a debug package

  : Variables available in `debian/rules` with debhelper rules

Extra targets can be used in `debian/rules` with these rules (called in
this order):

  Target                                                                                                                          Usage
  ------------------------------------------------------------------------------------------------------------------------------- ----------------------------------------------------------------------------------------------------------------------------
  **targets called just before 'common-install-prehook-arch' and 'common-install-prehook-indep' common targets, in this order**
  common-install-prehook-impl                                                                                                     actions before installation and after debhelper prepared the directories
  **targets called just before 'build/package' common targets, in this order**
  binary-install/package                                                                                                          actions after the upstream build system and debhelper installed everything
  binary-post-install/package                                                                                                     actions after everything is installed (after the 'binary-install' rule, which may be customized in a class or by yourself)
  binary-strip/package                                                                                                            actions after stripping executables and libraries
  binary-fixup/package                                                                                                            actions after gzipping certain files (doc, examples, ...) and fixing permissions
  binary-predeb/package                                                                                                           actions just before creating the debs
  binary-makedeb/package                                                                                                          actions just after creating the debs

  : Targets available in `debian/rules` with debhelper rules

For example, to add post install preparation (after debhelper work):
binary-install/foo:: chmod a+x debian/foo/usr/bin/pouet

These rules handles the following debhelper commands for each binary
package automatically (with the parameters you can use to customize
their behavior):

  Commands                                                                      Parameters
  ----------------------------------------------------------------------------- ------------------------------------------------------------------------------------------------------------------- ---------------------------------------------------------------------------------------------------------------------------------------------------------------------
  **commands called during 'common-install-prehook-impl rule, in this order**
  `dh_clean`[^7]                                                                `DEB_CLEAN_EXCLUDE`                                                                                                 regular expressions matching files which should not be cleaned
  `dh_prep`[^8]                                                                 `DEB_CLEAN_EXCLUDE`                                                                                                 regular expressions matching files which should not be cleaned
  `dh_installdirs`                                                              `DEB_INSTALL_DIRS_ALL`                                                                                              subdirectories to create in installation staging directory, for all packages
  **commands called during 'install/package' rules, in this order**
  `dh_installdirs`                                                              `DEB_INSTALL_DIRS_`                                                                                                 subdirectories to create in installation staging directory, for package package
  **commands called during 'binary-install/package' rules, in this order**
  `dh_installdocs`                                                              `DEB_INSTALL_DOCS_ALL`                                                                                              files which should go in `/usr/share/doc/`, for all packages
  `DEB_INSTALL_DOCS_`                                                           files which should go in `/usr/share/doc/`, for package package
  `dh_installexamples`                                                          `DEB_INSTALL_EXAMPLES_`                                                                                             files which should go in `/usr/share/doc//examples`, for package package
  `dh_installman`                                                               `DEB_INSTALL_MANPAGES_`                                                                                             manpages to install in the package, for package package
  `dh_installinfo`                                                              `DEB_INSTALL_INFO_`                                                                                                 info files which should go in `/usr/share/info`, for package package
  `dh_installmenu`                                                              `DEB_DH_INSTALL_MENU_ARGS`                                                                                          extra arguments passed to the command
  `dh_installcron`                                                              `DEB_DH_INSTALL_CRON_ARGS`                                                                                          extra arguments passed to the command
  `dh_installinit`                                                              `DEB_UPDATE_RCD_PARAMS_`                                                                                            arguments passed to update-rc.d in init scripts, for package package
  `DEB_UPDATE_RCD_PARAMS`                                                       arguments passed to update-rc.d in init scripts, for all packages (overrides `DEB_UPDATE_RCD_PARAMS_` parameters)
  `DEB_DH_INSTALLINIT_ARGS`                                                     extra arguments passed to the command
  `dh_installdebconf`                                                           `DEB_DH_INSTALLDEBCONF_ARGS`                                                                                        extra arguments passed to the command
  `dh_installemacsen`                                                           `DEB_EMACS_PRIORITY`                                                                                                overrides the default priority for the site-start.d file
  `DEB_EMACS_FLAVOR`                                                            overrides the default Emacs flavor for the site-start.d file
  `DEB_DH_INSTALLEMACSEN_ARGS`                                                  extra arguments passed to the command
  `dh_installcatalogs`                                                          `DEB_DH_INSTALLCATALOGS_ARGS`                                                                                       extra arguments passed to the command
  `dh_installpam`                                                               `DEB_DH_INSTALLPAM_ARGS`                                                                                            extra arguments passed to the command
  `dh_installlogrotate`                                                         `DEB_DH_INSTALLLOGROTATE_ARGS`                                                                                      extra arguments passed to the command
  `dh_installlogcheck`                                                          `DEB_DH_INSTALLLOGCHECK_ARGS`                                                                                       extra arguments passed to the command
  `dh_installchangelogs`                                                        `DEB_INSTALL_CHANGELOGS_ALL`                                                                                        files which should be interpreted as upstream changelog, for all packages
  `DEB_INSTALL_CHANGELOGS_`                                                     files which should be interpreted as upstream changelog, for package package
  `DEB_DH_INSTALLCHANGELOGS_ARGS`                                               extra arguments passed to the command
  `dh_installudev`                                                              `DEB_DH_INSTALLUDEV_ARGS`                                                                                           extra arguments passed to the command
  `dh_lintian`                                                                  `DEB_DH_LINTIAN_ARGS`                                                                                               extra arguments passed to the command
  `dh_install`                                                                  `DEB_DH_INSTALL_SOURCEDIR`[^9]                                                                                      directory where to look for files to install
  `DEB_DH_INSTALL_ARGS`                                                         extra arguments passed to the command
  `dh_link`                                                                     `DEB_DH_LINK_ARGS`                                                                                                  extra arguments passed to the command, for all packages
  `DEB_DH_LINK_`                                                                extra arguments passed to the command, for package package
  `dh_installmime`                                                              `DEB_DH_INSTALLMIME_ARGS`                                                                                           extra arguments passed to the command
  **commands called during 'binary-strip/package' rules, in this order**
  `dh_strip`[^10]                                                               `DEB_STRIP_EXCLUDE`                                                                                                 regular expressions matching files which should not be stripped
  `DEB_DH_STRIP_ARGS`                                                           extra arguments passed to the command
  **commands called during 'binary-fixup/package' rules, in this order**
  `dh_compress`                                                                 `DEB_COMPRESS_EXCLUDE`                                                                                              regular expressions matching files which should not be compressed
  `dh_fixperms`                                                                 `DEB_FIXPERMS_EXCLUDE`                                                                                              regular expressions matching files which should not have their permissions changed
  `DEB_DH_FIXPERMS_ARGS`                                                        extra arguments passed to the command
  `dh_makeshlibs`                                                               `DEB_DH_MAKESHLIBS_ARGS_ALL`                                                                                        extra arguments passed the command, for all packages
  `DEB_DH_MAKESHLIBS_ARGS_`                                                     extra arguments passed to the command, for package package
  `DEB_DH_MAKESHLIBS_ARGS`                                                      completely override argument passed to the command
  **commands called during 'binary-predeb/package' rules, in this order**
  `dh_installdeb`                                                               `DEB_DH_INSTALLDEB_ARGS`                                                                                            extra arguments passed to the command
  `dh_perl`                                                                     `DEB_PERL_INCLUDE`                                                                                                  space-separated list of paths to search for perl modules, for all packages
  `DEB_PERL_INCLUDE_`                                                           space-separated list of paths to search for perl modules, for package package
  `DEB_DH_PERL_ARGS`                                                            completely override argument passed to the command
  `dh_shlibdeps`                                                                `DEB_SHLIBDEPS_LIBRARY_`[^11]                                                                                       if set, look first in the package build directory for the specified package name, when searching for libraries, symbol files, and shlibs files, for package package
  `DEB_SHLIBDEPS_INCLUDE`[^12]                                                  space-separated list of paths to search first for dependency info, for package package
  `DEB_SHLIBDEPS_INCLUDE_$`                                                     space-separated list of paths to search first for dependency info, for package package
  `DEB_DH_SHLIBDEPS_ARGS_ALL`                                                   extra arguments passed the command, for all packages
  `DEB_DH_SHLIBDEPS_ARGS_`                                                      extra arguments passed to the command, for package package
  `DEB_DH_SHLIBDEPS_ARGS`                                                       completely override argument passed to the command
  **commands called during 'binary-makedeb/package' rules, in this order**
  `dh_gencontrol`                                                               `DEB_DH_GENCONTROL_ARGS_ALL`                                                                                        extra arguments passed the command, for all packages
  `DEB_DH_GENCONTROL_ARGS_`                                                     extra arguments passed to the command, for package package
  `DEB_DH_GENCONTROL_ARGS`                                                      completely override argument passed to the command
  `dh_md5sums`                                                                  `DEB_DH_MD5SUMS_ARGS`                                                                                               extra arguments passed the command
  `dh_builddeb`                                                                 `DEB_DH_BUILDDEB_ENV`                                                                                               environment passed to the command (pass DH\_ALWAYS\_EXCLUDE by default)
  `DEB_DH_BUILDDEB_ARGS`                                                        extra arguments passed to the command
  **commands called during 'clean' rule, in this order**
  `dh_clean`                                                                    `DEB_CLEAN_EXCLUDE`                                                                                                 regular expressions matching files which should not be cleaned

  : Debhelper commands commonly managed

Other debhelper commands can be handled in specific classes or may be
called in custom rules.

> **Important**
>
> If `debian/control` management is activated (see below), build
> dependency on debhelper is automatically added, if not, you will have
> to do it yourself.
>
> Having a versioned dependency on debhelper is recommended as it will
> ensure people will use the version providing the necessary features
> (CDBS `debian/control` management will do it).

### Debhelper customization examples

To specify a tight dependency on a package containing shared libraries:
DEB\_DH\_MAKESHLIBS\_ARGS\_libfoo := -V"libfoo (\>= 0.1.2-3)"
DEB\_SHLIBDEPS\_LIBRARY\_arkrpg := libfoo
DEB\_SHLIBDEPS\_INCLUDE\_arkrpg := debian/libfoo/usr/lib/

To install a changelog file with an uncommon name as
`ProjectChanges.txt.gz`: DEB\_INSTALL\_CHANGELOGS\_ALL :=
ProjectChanges.txt

To avoid compressing files with i`.py` extension: DEB\_COMPRESS\_EXCLUDE
:= .py

To register a debug library package libfoo-dbg for libfoo (which needs
unstripped `.so`) in compat mode 4: DEB\_DH\_STRIP\_ARGS :=
--dbg-package=libfoo Starting from compat mode 5, CDBS automatically
detect -dbg packages and pass the needed arguments to `dh_strip`;
`DEB_DH_STRIP_ARGS` can still be useful to pass additional parameters
like excluded items (`--exclude=`).

Perl-specific debhelper options (`dh_perl` call is always performed): \#
Add a space-separated list of paths to search for perl modules
DEB\_PERL\_INCLUDE := /usr/lib/perl-z \# Like the above, but for the
'libperl-stuff' package DEB\_PERL\_INCLUDE\_libperl-stuff :=
/usr/lib/perl-plop \# Overrides options passed to dh\_perl
DEB\_DH\_PERL\_ARGS := -d

To avoid loosing temporary generated files in `dh_clean` processing
(rarely useful): \# files containing these pattern would not be deleted
\# (beware CDBS changelog has a typo while highlighting new
DEB\_CLEAN\_EXCLUDE\*S\* feature) DEB\_CLEAN\_EXCLUDE := precious keep

Cross-Building
--------------

Since version 0.4.83, CDBS has a working cross-building support. If
cross-building, a few adaptations are automatically made:

-   the `cdbs_crossbuild` variable is automatically set (and can be used
    in `debian/rules`)

-   in the makefile class: CC is passed to the make calls with the value
    "\$(`DEB_HOST_GNU_TYPE`)-gcc"

-   in the autotools class: CC and CXX are only passed to the configure
    call if explicitly set in environment or `debian/rules`

Specific Tasks
==============

Packaging applications with a generic build-system
--------------------------------------------------

### Class for Makefile

This class is for the guys who only have a Makefile (no autotools
available) to build the program. You only need to have four rules in the
Makefile:

-   one for cleaning the build directory (i.e. mrproper)

-   one for building your software (i.e. myprog)

-   one for checking if the software is working properly (i.e. check)

-   one for installing your software (i.e. install)

To be honest, the install rules is not a must-have, but it always helps
a lot when you've got it.

#### Classic Build

The first operation, is to write the `debian/rules`. First, we add the
include lines: include /usr/share/cdbs/1/class/makefile.mk

Now, it remains to tell cdbs the name of our four Makefile rules. For
the previous examples it gives: DEB\_MAKE\_CLEAN\_TARGET := mrproper \#
if you detect authors's loss of sanity, tell CDBS not to try running the
nonexistent clean rule, and do the job yourself in `debian/rules`
DEB\_MAKE\_CLEAN\_TARGET := DEB\_MAKE\_BUILD\_TARGET := myprog
DEB\_MAKE\_INSTALL\_TARGET := install DESTDIR=\$(CURDIR)/debian/tmp/ \#
no check for this software DEB\_MAKE\_CHECK\_TARGET := \# allow changing
the makefile filename in case of emergency exotic practices
DEB\_MAKE\_MAKEFILE := MaKeFiLe \# example when changing environment
variables is necessary: DEB\_MAKE\_ENVVARS :=
CFLAGS="-fomit-frame-pointer"

`DEB_BUILD_OPTIONS` is checked for the following options:

-   noopt: use -O0 instead of -O2

-   nocheck: skip the check rule

If your Makefile doesn't support the `DESTDIR` variable, take a look in
it and find the variable responsible for setting installation directory.
If you don't find some variable to do this, you'll have to patch the
file...

That's all :).

#### Multiple "flavors" Build

After setting the basic parameters like for a classic build, you just
need to specify the list of your "flavors" and ensure you've got a
specific build directory (which can't be the same as the source
directory or all builds would walk over themselves in a big mess): \#
needs to be declared before including makefile.mk DEB\_MAKE\_FLAVORS =
full nox light DEB\_BUILDDIR = build

If you need to create customized rules like this:
\$(cdbs\_make\_clean\_nonstamps):: \$(if \$(cdbs\_make\_flavors),rm -f
extra-stuff-\$(cdbs\_make\_curflavor)) \$(cdbs\_make\_install\_stamps)::
\$(if \$(cdbs\_make\_flavors),dostuff --with=\$(cdbs\_make\_curflavor))
you can use these extra CDBS variables:

-   `cdbs_make_flavors`: sorted flavor list (nonempty if multibuild
    activated)

-   `cdbs_make_curflavor`: current flavor for this build

-   `cdbs_make_curbuilddir`: build directory for the current flavor

-   `cdbs_make_curdestdir`: installation directory for the current
    flavor

### Class for Autotools

This class is able to use configure scripts and makefiles generated with
autotools (and possibly libtool). All rules are called automatically and
clean rules to remove generated files during build are also added. This
class in fact improves the makefile class to support autotools features
and provide good defaults.

#### Classic Build

To use it, just add this line to your `debian/rules` include
/usr/share/cdbs/1/class/autotools.mk

CDBS automatically handles common flags to pass to the configure script,
but it is possible to give some extra parameters:
DEB\_CONFIGURE\_EXTRA\_FLAGS := --with-ipv6 --with-foo

If the build system uses non-standard configure options you can override
CDBS default behavior: COMMON\_CONFIGURE\_FLAGS := --program-dir=/usr
(notice that `DEB_CONFIGURE_EXTRA_FLAGS` would still be appended)

If some specific environment variables need to be setup, use:
DEB\_CONFIGURE\_SCRIPT\_ENV += BUILDOPT="someopt"

> **Warning**
>
> Prefer use of += instead of := not to override other environment
> variables (CC / CXX / CFLAGS / CXXFLAGS / CPPFLAGS / LDFLAGS)
> propagated in the CDBS default.

CDBS will automatically update `config.sub`, `config.guess`, and
`config.rpath` before build and restore the old ones at clean stage
(even if using the tarball system). If needed, and if `debian/control`
management is activated, autotools-dev and/or gnulib will then be
automatically added to the build dependencies (needed to find updated
versions of the files).

If the program does not use the top source directory to store autoconf
files, you can teach CDBS where it is to be found: DEB\_AC\_AUX\_DIR =
\$(DEB\_SRCDIR)/autoconf

CDBS can be asked to update libtool, autoconf, and automake files, but
this behavior is likely to break the build system and is *STRONGLY*
discouraged. Nevertheless, if you still want this feature, set the
following variables:

-   `DEB_AUTO_UPDATE_LIBTOOL`: `pre` to call libtoolize, or `post` to
    copy system-wide `libtool` after configure is done

-   `DEB_AUTO_UPDATE_ACLOCAL`: `aclocal` version to use

-   `DEB_AUTO_UPDATE_AUTOCONF`: `autoconf` version to use

-   `DEB_AUTO_UPDATE_AUTOHEADER`: `autoheader` version to use

-   `DEB_AUTO_UPDATE_AUTOMAKE`: `automake` version to use

-   `DEB_ACLOCAL_ARGS`: extra arguments to `aclocal` call (defaults to
    `/m4` if exists)

-   `DEB_AUTOMAKE_ARGS`: extra arguments to `automake` call

(corresponding build dependencies will automatically be added)

The following make parameters can be overridden: \# these are the
defaults CDBS provides DEB\_MAKE\_INSTALL\_TARGET := install
DESTDIR=\$(DEB\_DESTDIR) DEB\_MAKE\_CLEAN\_TARGET := distclean
DEB\_MAKE\_CHECK\_TARGET := \# example to work around dirty makefile
DEB\_MAKE\_INSTALL\_TARGET := install prefix=\$(CURDIR)/debian/tmp/usr
\# example with nonexistent install rule for make
DEB\_MAKE\_INSTALL\_TARGET := \# example to activate check rule
DEB\_MAKE\_CHECK\_TARGET := check \# overriding make-only environment
variables : \# (should never be necessary in a clean build system) \#
(example borrowed from the bioapi package) DEB\_MAKE\_ENVVARS :=
"SKIPCONFIG=true"

`DEB_BUILD_OPTIONS` is checked for the following options:

-   noopt: use -O0 instead of -O2

-   nocheck: skip the check rule

If you are using CDBS version \< 0.4.39, it automatically cleans
autotools files generated during build (`config.cache`, `config.log`,
and `config.status`). Since version 0.4.39, CDBS leave them all
considering it is not his job to correct an upstream buildsys
misbehavior (but you may remove them in the clean rule if necessary
before you get the issue solved by authors).

#### Multiple "flavors" Build

The basics are the same as [in the makefile
class](#makefile-class-multibuild) (on which the autotools class
depends). Combined with this class you can do interesting customizations
like this: \# global configure options DEB\_CONFIGURE\_EXTRA\_FLAGS =
--enable-gnutls \# per flavor configure options
DEB\_CONFIGURE\_EXTRA\_FLAGS\_full = --enable-gtk --enable-pam
DEB\_CONFIGURE\_EXTRA\_FLAGS\_nox = --enable-pam
DEB\_CONFIGURE\_EXTRA\_FLAGS\_light = --disable-unicode
DEB\_CONFIGURE\_EXTRA\_FLAGS +=
\$(DEB\_CONFIGURE\_EXTRA\_FLAGS\_\$(cdbs\_make\_curflavor))

### Class for CMake

This class is intended to handle build systems using
[CMake](http://www.cmake.org/).

To use this class, add this line to your `debian/rules`: include
/usr/share/cdbs/1/class/cmake.mk

This class is an extension of the Makefile class, calling `cmake` in
`DEB_SRCDIR` with classic parameters and then calling `make`. In the
call to `cmake`, the install prefix and path to CC/CXX as well as
CFLAGS/CXXFLAGS are given automatically. You can pass extra arguments
using `DEB_CMAKE_EXTRA_FLAGS`, and in strange cases where you need a
special configuration you can override the default arguments using
`DEB_CMAKE_NORMAL_ARGS`.

### Class for qmake

This class is intended to handle build systems using
[qmake](http://doc.trolltech.com/4.5/qmake-manual.html), mostly used to
build Qt applications.

To use this class, add this line to your `debian/rules`: include
/usr/share/cdbs/1/class/qmake.mk

This class is an extension of the Makefile class, calling `qmake` in
`DEB_BUILDDIR` with classic parameters and then calling `make`. In the
call to `qmake`, the path to CC/CXX as well as
CPPFLAGS/CFLAGS/CPPFLAGS/CXXFLAGS are given automatically. Configuration
options can be given using `DEB_QMAKE_CONFIG_VAL` (resulting in adding
content to `CONFIG`), the later defaulting to `nostrip` if this option
is set in `DEB_BUILD_OPTIONS`.

### Class for SCons

This class is intended to handle build systems using
[SCons](http://www.scons.org/), which is an alternative to make.

To use this class, add this line to your `debian/rules`: include
/usr/share/cdbs/1/class/scons.mk

The class takes care to invoke SCons and cleanup things afterwards. You
can setup a few parameters: \# options for ALL targets (in addition to
the DEB\_SCONS\_\*\_OPTIONS variables) DEB\_SCONS\_OPTIONS = useALSA=1
\# you can override the default build target (empty)
DEB\_SCONS\_BUILD\_TARGET = \# options for the build target
DEB\_SCONS\_BUILD\_OPTIONS = useGettext=1 \# you can override the
default install target ("install") DEB\_SCONS\_INSTALL\_TARGET = install
devel-install \# options for the install target
DEB\_SCONS\_INSTALL\_OPTIONS = instdir=\$(CURDIR)/debian/tmp \# you can
override the default clean target (.) DEB\_SCONS\_CLEAN\_TARGET =
fullclean \# activate the check target if set to the target name
DEB\_SCONS\_CHECK\_TARGET = testsuite

Packaging Perl applications
---------------------------

### Subclass for Makefile (ExtUtils::MakeMaker)

> **Warning**
>
> This class replaces the 'perlmodule' class, which is deprecated. This
> one is very similar: rules and parameters are almost the same, and
> only the `DEB_MAKEMAKER_PACKAGE` variable is deprecated, so the
> conversion should be pretty easy.

This class can manage Perl builds using
[ExtUtils::MakeMaker](http://perldoc.perl.org/ExtUtils/MakeMaker.html)
(Makefile.PL) automatically.

To use this class, add this line to your `debian/rules`: include
/usr/share/cdbs/1/class/perl-makemaker.mk

You can customize build options like this: \# add custom MakeMaker
options DEB\_MAKEMAKER\_USER\_FLAGS = --with-ipv6

As this class is a subclass of the makefile class, you can override the
`DEB_MAKE_` variables, but it should not be needed. Also beware
`DEB_BUILDDIR` must match `DEB_SRCDIR` for this build system.

### Class for Module::Build

This class can manage Perl builds using
[Module::Build](http://perldoc.perl.org/Module/Build.html) (Build.PL)
automatically.

The following parameters can be overridden: \# these are the defaults
CDBS provides DEB\_PERL\_BUILD\_TARGET = build DEB\_PERL\_CHECK\_TARGET
= test DEB\_PERL\_INSTALL\_TARGET = install DEB\_PERL\_CLEAN\_TARGET =
realclean \# add your custom parameters if the defaults do not suits you
DEB\_PERL\_CHECK\_FLAGS = fulltestsuite=1 DEB\_PERL\_CONFIGURE\_FLAGS =
destdir=\$(CURDIR)/build/stuff

Also beware `DEB_BUILDDIR` must match `DEB_SRCDIR` for this build
system.

### Customizations common to all Perl classes

If you included the debhelper class, it can take care of calling
`dh_perl`. Have a look at Perl-specific debhelper options described
[here](#debhelper-commands).

Extra variables you can use in your `debian/rules`:

-   `DEB_PERL_PACKAGES`: list of Perlpackages

-   `DEB_PERL_ARCH_PACKAGES`: list of architecture dependent
    Perlpackages

-   `DEB_PERL_INDEP_PACKAGES`: list of architecture independent
    Perlpackages

> **Important**
>
> If `debian/control` management is activated (see below), build
> dependency on perl is automatically added, if not, you will have to do
> it yourself.

Packaging Python applications
-----------------------------

With the new policy all versioned packages (pythonver-app) are collapsed
into a single package (python-app). The Python classes are able to move
python scripts and .so files in the new locations automatically. You can
use the auto control file generation feature to ensure your
Build-Depends are set correctly for the new needed tools. The classes
also remove `.egg-info` directories sometimes left over by Python
distutils.

Before choosing the right class to use (depending on the build system
used upstream, and described in the next sections), you need to choose a
Python package manager. You may wonder why this choice at all, and why
Debian Python folks did not stick to a single common tool, but this is a
long tragedy, and you'd find most of it on the Debian mailing-lists, if
you're curious. You can find more information on these tools on [this
page](http://wiki.debian.org/DebianPythonFAQ) and in the corresponding
packages.

Then, add the following *before* the Python class inclusion to your
`debian/rules`: \# select the python system you want to use : pysupport
or pycentral \# (this MUST be done before including the class)
DEB\_PYTHON\_SYSTEM = pysupport

It can handle the following debhelper commands automagically:

  Commands                                                                        Parameters
  ------------------------------------------------------------------------------- ---------------------------------------------------------- -------------------------------------------------------
  **commands called during 'binary-post-install/package' rules, in this order**
  `dh_pysupport` or `dh_pycentral`.                                               `DEB_PYTHON_PRIVATE_MODULES_DIRS`[^13]                     list of private modules directories, for all packages
  `DEB_PYTHON_PRIVATE_MODULES_DIRS_`                                              list of private modules directories, for package package

  : Debhelper commands managed for Python packaging

### Class for Python distutils

This class can manage common Python builds using
[distutils](http://docs.python.org/distutils/) automatically.

> **Warning**
>
> Since 0.4.53, this class does not handle old-policy packages
> (pre-Etch) anymore.

To use this class, add these lines to your `debian/rules`: \# Don't
forget to declare DEB\_PYTHON\_SYSTEM before this line include
/usr/share/cdbs/1/class/python-distutils.mk

You can customize build options like this: \#\#\#\#\#\#\#\#\#\#\#\#\#\#
These variables have not changed (same in the old and new policy) \#
change the Python build script name (default is 'setup.py')
DEB\_PYTHON\_SETUP\_CMD := install.py \# clean options for the Python
build script DEB\_PYTHON\_CLEAN\_ARGS = -all \# build options for the
Python build script DEB\_PYTHON\_BUILD\_ARGS =
--build-base="\$(DEB\_BUILDDIR)/specific-build-dir" \# common additional
install options for all binary packages \# ('--root' option is always
set) DEB\_PYTHON\_INSTALL\_ARGS\_ALL = --no-compile --optimize --force

Complete `debian/rules` example using python-support for a module
(editobj): \#!/usr/bin/make -f \# -\*- mode: makefile; coding: utf-8
-\*- include /usr/share/cdbs/1/rules/debhelper.mk DEB\_PYTHON\_SYSTEM =
pysupport include /usr/share/cdbs/1/class/python-distutils.mk include
/usr/share/cdbs/1/rules/patchsys-quilt.mk DEB\_COMPRESS\_EXCLUDE := .py
\$(patsubst %,install/%,\$(cdbs\_python\_packages)) mv
debian/\$(cdbs\_curpkg)/usr/lib/python\*/site-packages/editobj/icons \\
debian/\$(cdbs\_curpkg)/usr/share/\$(cdbs\_curpkg) \$(patsubst
%,binary-install/%,\$(cdbs\_python\_packages)) find
debian/\$(cdbs\_curpkg)/usr/share/ -type f -exec chmod -R a-x {} \\;

### Subclass for autotools

For a Python module using autotools, it may be convenient to use this
class, which is a subclass of the generic autotools class. It currently
complement the `DEB_CONFIGURE_SCRIPT_ENV` variable with the `PYTHON`
parameter, and adds needed build dependencies if `debian/control`
management is activated. Moreover, if the package is to be built for
multiple Python versions, then the multiple "flavors" build is
automatically activated (`DEB_MAKE_FLAVORS` is set accordingly, but
still can be overridden is needed).

To use this class, add this line to your `debian/rules`: include
/usr/share/cdbs/1/class/python-autotools.mk

### Class for Python Sugar

This class can manage activity extensions for the [Sugar Learning
Platform](http://sugarlabs.org/) (originally developed for the One
Laptop per Child XO-1 netbook) automatically.

To use this class, add these lines to your `debian/rules`: include
/usr/share/cdbs/1/class/python-sugar.mk \# Compulsory: Sugar branches
supported by this activity extension DEB\_SUGAR\_BRANCHES = 0.84 0.86

### Customizations common to all Python classes

You can customize build options like this: \#\#\#\#\#\#\#\#\#\#\#\#\#\#
These variables are additions for the new policy \# packages holding the
collapsed content of all supported versions \# CDBS defaults to the
first non -doc/-dev/-common package listed in "debian/control" \# this
variable allows to override automatic detection
DEB\_PYTHON\_MODULE\_PACKAGES = mypyapp \# list of private modules
directories (for all binary packages) (needed to automatically handle
bytecompilation) DEB\_PYTHON\_PRIVATE\_MODULES\_DIRS =
/usr/share/mypyapp/my-pv-module \# or for a specific binary package
DEB\_PYTHON\_PRIVATE\_MODULES\_DIRS\_python-stuff-client =
/usr/share/mypyapp/client-pv-module \# overrides the default Python
installation root directory DEB\_PYTHON\_DESTDIR =
\$(CURDIR)/debian/python-stuff

You may use some read-only (meaning you MUST NOT alter them) variables
in your `debian/rules`:

-   `cdbs_python_current_version`: current python version number
    (defined only if selected package is a module)

-   `cdbs_python_build_versions`: list of space separated version
    numbers for which the selected module/extension is gonna be built

-   `cdbs_python_destdir`: Python installation root directory (works
    even when you didn't used `DEB_PYTHON_DESTDIR`

-   `cdbs_python_packages`: list of all Python packages

-   `cdbs_python_arch_packages`: list of all Python architecture
    dependent packages

-   `cdbs_python_indep_packages`: list of all Python architecture
    independent packages

> **Warning**
>
> Do not use the `DEB_PYTHON_MODULE_PACKAGE` variable anymore, it has
> been obsoleted. Use the `DEB_PYTHON_MODULE_PACKAGES` to force the list
> of module packages, or `cdbs_python_packages` variable in rules
> generation.
>
> Do not use the `cdbs_python_support_path` and
> `cdbs_python_module_arch` variables anymore, they have been obsoleted.
> `cdbs_python_support_path` is not useful anymore, just install files
> in the standard way and python-support would do the rest (messing into
> python-support internals was only a workaround when it was
> incomplete). `cdbs_python_module_arch` can easily be replace by a rule
> rewrite or a membership test using `cdbs_python_arch_packages` or
> `cdbs_python_indep_packages`.

Packaging Ruby applications
---------------------------

### Class for Ruby setup.rb

This class can manage common setup.rb installer automatically.

To use this class, install the ruby-pkg-tools package, and add this line
to your `debian/rules`: include
/usr/share/ruby-pkg-tools/1/class/ruby-setup-rb.mk

It can handle the following debhelper commands automagically:

  Commands                                                                   Parameters
  -------------------------------------------------------------------------- ------------ --
  **commands called during 'binary-install/package' rules, in this order**
  `dh_rdoc`[^14]                                                                          

  : Debhelper commands managed for Ruby packaging

Most ruby packages are architecture all, and then don't need being build
for multiple ruby versions; your package should then be called
'libfoo-ruby' or 'foo' and CDBS would automatically use the current
Debian ruby version to build it. If your package contains a compiled
part or a binding to an external lib, then you will have packages named
'libfoo-ruby1.6', 'libfoo-ruby1.8', and so on, then CDBS would
automatically build each package with the corresponding ruby version. In
this case, don't forget to add a 'libfoo-ruby' convenience dummy package
depending on the current Debian ruby version. If you have documentation
you want split into a separate package, then call it 'libfoo-ruby-doc'.
If this is Rdoc documentation, you may want to include the debhelper
class, as explained before, to have it generated and installed
automagically.

You can customize build options like this: \# force using a specific
ruby version for build \# (should not be necessary) DEB\_RUBY\_VERSIONS
:= 1.9 \# use ancestor DEB\_RUBY\_SETUP\_CMD := install.rb \# config
options for the ruby build script \# (older setup.rb used --site-ruby
instead of --siteruby) DEB\_RUBY\_CONFIG\_ARGS =
--site-ruby=/usr/lib/ruby/1.9-beta

### Rules for the Ruby Extras Team

> **Warning**
>
> The uploaders rule is *deprecated*. It has been decided Uploaders
> should only be people who made significant changes to the package (and
> then added manually). Read [this
> thread](http://lists.alioth.debian.org/pipermail/pkg-ruby-extras-maintainers/2007-February/001426.html)
> for more information.

If you are part of the Ruby Extras Team, or having the Team as
Uploaders, and you feel bored maintaining the list of developers, this
rule is made for you.

To use this class, install the ruby-pkg-tools package, and add this line
to your `debian/rules`: include
/usr/share/ruby-pkg-tools/1/rules/uploaders.mk

Rename your `debian/control` file to `debian/control.in` and run the
clean rule (`./debian/rules clean`) to regenerate the `debian/control`
file, replacing the `@RUBY_EXTRAS_TEAM@` (`@RUBY_TEAM@` is deprecated)
tag with the list of developers automatically.

Packaging GNOME applications
----------------------------

### Class for GNOME

> **Important**
>
> This class is intended to make packaging GNOME software easily,
> managing specific actions for you. This documentation describe the
> GNOME class, which is shipped with CDBS, but if you intend to work on
> this area seriously, you should also have a look at the GNOME Team
> rules in the next chapter.

This class adds a make environment variable :
GCONF\_DISABLE\_MAKEFILE\_SCHEMA\_INSTALL = 1 (''This is necessary
because the Gconf schemas have to be registered at install time. In the
case of packaging, this registration cannot be done when building the
package, so this variable disable schema registration in `make install`.
This procedure if deferred until gconftool-2 is called in
`debian/postinst` to register them, and in `debian/prerm` to unregister
them. The `dh_gconf` script is able to add the right rules automatically
for you.'')

It can handle the following debhelper commands automagically:

  Commands                                                                   Parameters
  -------------------------------------------------------------------------- ---------------------------- ---------------------------------------
  **commands called during 'binary-install/package' rules, in this order**
  `dh_scrollkeeper`                                                          `DEB_DH_SCROLLKEEPER_ARGS`   extra arguments passed to the command
  `dh_gconf`                                                                 `DEB_DH_GCONF_ARGS`          extra arguments passed to the command
  `dh_icons`                                                                 `DEB_DH_ICONS_ARGS`          extra arguments passed to the command

  : Debhelper commands managed for GNOME packaging

Moreover it adds some more clean rules to remove:

-   intltool generated files

-   scrollkeeper generated files (left over `.omf.out` files in `doc`
    and `help` directories)

To use it, just add this line to your `debian/rules`, after the
debhelper class include: include /usr/share/cdbs/1/class/gnome.mk

For more information on GNOME specific packaging rules, look at the
[Debian GNOME packaging
policy](http://alioth.debian.org/docman/view.php/30194/18/gnome-policy-20030502-1.html).

### Rules for the GNOME Team

If you install the extra gnome-pkg-tools package, you will have extra
rules and tools used by the GNOME Team for their packages. You should
really have a look to the package documentation for additional
information (mostly in
`/usr/share/doc/gnome-pkg-tools/README.Debian.gz`).

If you are part of the GNOME Team, or having the Team as Uploaders, and
you feel bored maintaining the list of uploaders, add this line to your
`debian/rules`: include /usr/share/gnome-pkg-tools/1/rules/uploaders.mk

Rename your `debian/control` file to `debian/control.in` and run the
clean rule (`./debian/rules clean`) to regenerate the `debian/control`
file, replacing the '@GNOME\_TEAM@' tag with the list of developers
automatically.

> **Warning**
>
> If you are using the `debian/control` file management, please note
> this rule will override this feature To cope with this problem,
> allowing at least Build-Depends handling, use the following
> work-around (until it is solved in a proper way): ifneq (,
> \$(DEB\_AUTO\_UPDATE\_DEBIAN\_CONTROL)) clean:: sed -i
> "s/@cdbs@/\$(CDBS\_BUILD\_DEPENDS)/g" debian/control endif

Packaging KDE applications
--------------------------

> **Warning**
>
> This class is intended for KDE 3 only. If you want to package KDE 4
> components, please refer to the pkg-kde-tools package instead, which
> is maintained by the KDE Team (and especially read the notes in
> `/usr/share/doc/pkg-kde-tools/README.Debian`); we should describe it
> in this documentation later.

To use this class, add this line to your `debian/rules` file: include
/usr/share/cdbs/1/class/kde.mk

CDBS automatically exports the following variables with the right value:

-   kde\_cgidir (/usr/lib/cgi-bin)

-   kde\_confdir (/etc/kde3)

-   kde\_htmldir (/usr/share/doc/kde/HTML)

`DEB_BUILDDIR`, `DEB_AC_AUX_DIR` and `DEB_CONFIGURE_INCLUDEDIR` are set
to KDE defaults.

The following files are excluded from compression:

-   .dcl

-   .docbook

-   -license

-   .tag

-   .sty

-   .el

(take care of them if you override the `DEB_COMPRESS_EXCLUDE` variable)

It can handle configure options specific to KDE (not forgetting
disabling rpath and activating xinerama), set the correct autotools
directory, and launch make rules adequately.

You can enable APIDOX build by setting the `DEB_KDE_APIDOX` variable to
a non-void value.

you can enable the final mode build by setting `DEB_KDE_ENABLE_FINAL`
variable to a non-void value.

`DEB_BUILD_OPTIONS` is checked for the following options:

-   noopt: disable optimisations (and KDE final mode, overriding
    `DEB_KDE_ENABLE_FINAL`)

-   nostrip: enable KDE debug (and disable KDE final mode, overriding
    `DEB_KDE_ENABLE_FINAL`)

You can prepare the build using the 'buildprep' convenience target:
`fakeroot debian/rules buildprep` (which is in fact calling the dist
target of `admin/Makefile.common`).

Packaging Java applications
---------------------------

This class allows packaging [Java](http://www.java.com/) applications
using [Ant](http://ant.apache.org/) (a Java-based build tool).

To use this class, add this include to your `debian/rules` and set the
following variables: include /usr/share/cdbs/1/class/ant.mk \# Set
either a single (JAVA\_HOME) or multiple (JAVA\_HOME\_DIRS) java
locations JAVA\_HOME := /usr/lib/kaffe \# or set JAVACMD if you don't
use default '\<JAVA\_HOME\>/bin/java' path \#JAVACMD := /usr/bin/java \#
Set Ant location ANT\_HOME := /usr/share/ant-cvs

You may add additional JARs like in the following example: \# list of
additional JAR files ('.jar' extension may be omitted) \# (path must be
absolute of relative to '/usr/share/java') DEB\_JARS :=
/usr/lib/java-bonus/ldap-connector adml-adapter.jar

The property file defaults to `debian/ant.properties`.

You can provide additional JVM arguments using ANT\_OPTS. You can
provide as well additional Ant command line arguments using ANT\_ARGS
(global) and/or ANT\_ARGS\_pkg (for package pkg), thus overriding the
settings in `build.xml` and the property file.

CDBS will build and clean using defaults target from `build.xml`. To
override these rules, or run the install / check rules, set the
following variables to your needs: \# override build and clean target
DEB\_ANT\_BUILD\_TARGET = makeitrule DEB\_ANT\_CLEAN\_TARGET =
super-clean \# i want install and test rules to be run
DEB\_ANT\_INSTALL\_TARGET = install-all DEB\_ANT\_CHECK\_TARGET = check
\# extra settings DEB\_ANT\_BUILDFILE = debian/build.xml \# defaults to
\$(CURDIR)/debian/ant.properties DEB\_ANT\_PROPERTYFILE =
src/ant.properties DEB\_ANT\_ARGS = -Dpackage=awgraphapplet
-Dversion=\$(DEB\_NOEPOCH\_VERSION) DEB\_ANT\_COMPILER :=
org.eclipse.jdt.core.JDTCompilerAdapter

`DEB_BUILD_OPTIONS` is checked for the following options:

-   noopt: set 'compile.optimize' Ant option to false

You should be able to fetch some more information on this java-based
build tool in the [Ant Apache web site](http://ant.apache.org/).

Packaging Haskell applications
------------------------------

This class allows packaging [Haskell](http://haskell.org/) applications
using [HBuild](http://hackage.haskell.org/trac/hackage/wiki/HBuild) (the
Haskell mini-distutils). You should be able to fetch some more
information on Haskell distutils in [this
thread](http://www.haskell.org/pipermail/libraries/2003-July/001239.html).

CDBS can take care of -hugs and -ghc packages: invoke `Setup.lhs`
properly for clean and install part.

To use this class, add this line to your `debian/rules`: include
/usr/share/cdbs/1/class/hbuild.mk

Maintainer Tools
================

These are specials rules which can be activated by the Maintainer to do
extra checks or help automatize boring work, but having no direct link
with the software build itself.

Maintainer Mode
---------------

This mode aims at activating features which are not suitable for
automatic builds, but can help maintainers while they work on their
package. If `DEB_MAINTAINER_MODE` is set, this mode in activated and do
the following:

-   activate semi-automatic `debian/control` management
    (`DEB_AUTO_UPDATE_DEBIAN_CONTROL`)

-   activate a strict copyright check (if the utils rules are activated)

Semi-automatic `debian/control` management
------------------------------------------

> **Caution**
>
> Automatic `debian/control` generation using any tool is permitted into
> Debian as long as it is triggered manually by the developer and the
> latter checks the result carefully.
>
> Autogenerating `debian/control` without any human intervention could
> be harmful in some ways detailed in
> [\#311724](http://bugs.debian.org/311724). This is not allowed in
> Debian.
>
> We then urge you to avoid using `DEB_AUTO_UPDATE_DEBIAN_CONTROL`
> directly and instead invoke the autogeneration rules manually after
> you modified `debian/control.in` (this way users or buildds wouldn't
> have different Build-Depends when building, avoiding many problems).
> Do not forget to proofread the result before any upload.
>
> Manual `debian/control` regeneration:
> DEB\_AUTO\_UPDATE\_DEBIAN\_CONTROL=yes fakeroot debian/rules clean

This feature allow:

-   CDBS to automatically manage some build-related Build-Depends
    automatically

-   use of embedded shell commands

-   use of CPU and System criterias to specify architecture
    (*EXPERIMENTAL*)

Build-related Build-Depends are dependencies introduced by the use of
certain CDBS features, or auto-detected needs.

Embedded shell commands allows including hacks like: Build-Depends:
libgpm-dev [\`type-handling any linux-gnu\`]

CPU and System criterias implements support for Cpu/System fields, as a
replacement for the Architecture field (which is to be implemented in
dpkg in the long term, but still *EXPERIMENTAL*). Here is an example,
before: Architecture: all and after: Cpu: all System: all If these
fields are used, it is also possible to include special tags to easily
take advantage of the type-handling tool, like in this example:
Build-Depends: @cdbs@, procps [system: linux], plop [cpu: s390] (look at
the type-handling package documentation, for more information)

You can also change the way build-dependencies are separated inside the
Build-Depends field using the `CDBS_BUILD_DEPENDS_DELIMITER` variable.
It defaults recommended multiline style (see Debian Policy §7.1), but
can be changed using this variable, or get back to the old default
(single line coma-separated list) if unset.

1.  Rename `debian/control` into `debian/control.in`.

2.  Replace cdbs / debhelper / ... Build-Depends with @cdbs@ in your
    `debian/control.in` like this:

        Build-Depends-Indep: @cdbs@, python-dev (>= 2.3), python-soya (>= 0.9), \
           python-soya (<< 0.10), python-openal(>= 0.1.4-4), gettext
                                

3.  Then manually (re)generate `debian/control` as explained above (see
    the caution part).

Package Check Utilities
-----------------------

To use these utilities, add this line to your `debian/rules`: include
/usr/share/cdbs/1/rules/utils.mk

### Copyright Check

This rule uses `licensecheck` (provided by devscripts) to check the
`debian/copyright` file.

You need to create the empty file `debian/copyright_hints` (with touch)
to activate this feature and get a report at the beginning of the build.
If you set `DEB_COPYRIGHT_CHECK_STRICT`, the build will fail if it could
not be run or problems were found by `licensecheck`.

You can customize the `licensecheck` parameters using
`DEB_COPYRIGHT_CHECK_ARGS`, and the files to check for copyright
information using `DEB_COPYRIGHT_CHECK_REGEX` and
`DEB_COPYRIGHT_CHECK_IGNORE_REGEX`.

### Build Infos

This rule uses `dh_buildinfo` (provided by dh-buildinfo), a tool to
track package versions used to build a package.

It will generate a `debian/buildinfo.gz` file, which will be
automatically installed in `/usr/share/doc/`.

### List Missing Files

This rule is intended to avoid missing new files in a new software
version, to allow the maintainer to check the list of not-installed
files. This is useful when multiple binary packages are created and
things may be lost in `debian/tmp`.

The rule must be called manually, *after a build with the `-nc`
`dpkg-buildpackage` option* (to avoid cleaning the `debian` directory):
fakeroot debian/rules list-missing

It generates a list of installed files `debian/cdbs-install-list` found
in `debian/tmp` and a list of files installed in the package (or
ignored) `debian/cdbs-package-list`, then display the ones missing in
the package. You can add a list of files to ignored, because you want to
remember you wanted them excluded, in `debian/not-installed`. If you set
`DEB_BUILDINFO_STRICT`, the build will fail if it could not be
generated.

Upstream Source Management
--------------------------

This rule allows to fetch, repackage, possibly with files exclusion, the
upstream sources. You may also check the source tarball md5sum. It
provides a 'get-orig-source' target, as recommended by the Debian Policy
(chapter 4.9).

To use this rule, add these lines to your `debian/rules`: include
/usr/share/cdbs/1/rules/upstream-tarball.mk pkgbranch = 0.88 \#
Optional: you may override the default tarball name (defaults to
DEB\_SOURCE\_PACKAGE) DEB\_UPSTREAM\_PACKAGE =
\$(DEB\_SOURCE\_PACKAGE:%-\$(pkgbranch)=%) \# Compulsory
DEB\_UPSTREAM\_URL =
http://download.sugarlabs.org/sources/sucrose/glucose/\$(DEB\_UPSTREAM\_PACKAGE)
DEB\_UPSTREAM\_TARBALL\_EXTENSION = tar.bz2 \# Optional: you can request
a md5sum check DEB\_UPSTREAM\_TARBALL\_MD5 =
f50a666c4e1f55b8fc7650258da0c539 \# Optional: this list of
space-separated files and directories will be removed when repackaging
\# (Beware directories MUST end with a final '/' !!!)
DEB\_UPSTREAM\_REPACKAGE\_EXCLUDES = .pc/

You can then fetch/repackage the source tarball using
`fakeroot debian/rules get-orig-source`. If you specified a non-empty
`DEB_UPSTREAM_REPACKAGE_EXCLUDES`, you can check if the current tarball
is properly up-to-date and do not contain newly excluded patterns using
`fakeroot debian/rules fail-source-not-repackaged`.

Hall of examples
================

Java (with upstream-tarball + utils + debhelper)
------------------------------------------------

awstats
package
With this line in `debian/control.in`: Build-Depends: @cdbs@, sharutils
`debian/control` management gives in `debian/control`: Build-Depends:
cdbs (\>= 0.4.70\~), devscripts (\>= 2.10.7\~), dh-buildinfo, debhelper
(\>= 6), ant, default-jdk, sharutils

`debian/rules`: \#!/usr/bin/make -f \# -\*- mode: makefile; coding:
utf-8 -\*- \# Copyright © 2003-2010 Jonas Smedegaard \<dr@jones.dk\>
include /usr/share/cdbs/1/rules/upstream-tarball.mk include
/usr/share/cdbs/1/rules/utils.mk include /usr/share/cdbs/1/class/ant.mk
include /usr/share/cdbs/1/rules/debhelper.mk DEB\_UPSTREAM\_URL =
http://prdownloads.sourceforge.net/awstats
DEB\_UPSTREAM\_TARBALL\_BASENAME\_MANGLE =
s/(-6\\.9)\\.(\\d)/\$\$1\$\$2/ DEB\_UPSTREAM\_TARBALL\_MD5 =
26a5b19fa9f395e9e7dafed37b795d7f DEB\_UPSTREAM\_REPACKAGE\_EXCLUDE =
wwwroot/icon/browser/firefox.png DEB\_INSTALL\_CHANGELOGS\_ALL =
docs/awstats\_changelog.txt DEB\_INSTALL\_DOCS\_ALL = README.TXT
awstats\_example\_scripts = \$(filter-out
%/awstats\_buildstaticpages.pl,\$(wildcard tools/\*.pl)) \$(wildcard
debian/examples/\*.sh) DEB\_INSTALL\_EXAMPLES\_awstats =
\$(awstats\_example\_scripts) debian/examples/apache.conf
wwwroot/cgi-bin/awstats.model.conf wwwroot/cgi-bin/plugins/example/\*
wwwroot/css wwwroot/js tools/xslt DEB\_COMPRESS\_EXCLUDE = \$(notdir
\$(awstats\_example\_scripts)) awstats.ico JAVA\_HOME =
/usr/lib/jvm/default-java DEB\_ANT\_BUILDFILE = debian/build.xml
DEB\_ANT\_BUILD\_TARGET = bin-jar DEB\_ANT\_ARGS =
-Dpackage=awgraphapplet -Dversion=\$(DEB\_NOEPOCH\_VERSION) \#
"Binarize" (and cleanup) Debian-shipped non-trademarked Firefox icon
pre-build:: uudecode -o debian/icons/firefox.png
debian/icons/firefox.png.uu clean:: rm -f debian/icons/firefox.png \#
Adjust for Debian (and cleanup) main config file
common-configure-indep:: \# Use perl rather than a diff here, to make
sure all relevant \# options are checked (upstream defaults have moved
around in \# the past) \# Perl in shell in make requires extra care: \#
\* Single-quoting ('...') protects against shell expansion \# \*
Double-dollar (\$\$) expands to plain dollar (\$) in make perl -wp \\ -e
's,\^(LogFile\\s\*=\\s\*).\*,\$\$1"/var/log/apache2/access.log",;' \\ -e
's,\^(DirData\\s\*=\\s\*).\*,\$\$1"/var/lib/awstats",;' \\ -e
's,\^(LogFormat\\s\*=\\s\*).\*,\$\${1}4,;' \\ -e
's,\^(DNSLookup\\s\*=\\s\*).\*,\$\${1}1,;' \\ -e
's,\^(DirIcons\\s\*=\\s\*).\*,\$\$1"/awstats-icon",;' \\ -e
's,\^(HostAliases\\s\*=\\s\*).\*,\$\$1"localhost 127.0.0.1",;' \\ -e
's,\^(DirLang\\s\*=\\s\*).\*,\$\$1"/usr/share/awstats/lang",;' \\ -e
's,\^\#(LoadPlugin\\s\*=\\s\*"hashfiles"),\$\$1,;' \\ -e
's,\^\#(Include\\s\*)"",\$\$1"/etc/awstats/awstats.conf.local",;' \\ -e
's,\^(\#LoadPlugin\\s\*=\\s\*"geoip\\s\*GEOIP\_STANDARD\\s\*).\*,\$\$1/usr/share/GeoIP/GeoIP.dat",;'
\\ -e
's,\^(\#LoadPlugin\\s\*=\\s\*"geoip\_region\_maxmind\\s\*GEOIP\_STANDARD\\s\*).\*,\$\$1/usr/share/GeoIP/GeoIPRegion.dat",;'
\\ -e
's,\^(\#LoadPlugin\\s\*=\\s\*"geoip\_city\_maxmind\\s\*GEOIP\_STANDARD\\s\*).\*,\$\$1/usr/share/GeoIP/GeoIPCity.dat",;'
\\ -e
's,\^(\#LoadPlugin\\s\*=\\s\*"geoip\_isp\_maxmind\\s\*GEOIP\_STANDARD\\s\*).\*,\$\$1/usr/share/GeoIP/GeoIPISP.dat",;'
\\ -e
's,\^(\#LoadPlugin\\s\*=\\s\*"geoip\_org\_maxmind\\s\*GEOIP\_STANDARD\\s\*).\*,\$\$1/usr/share/GeoIP/GeoIPOrg.dat",;'
\\ \< wwwroot/cgi-bin/awstats.model.conf \> debian/awstats.conf clean::
rm -f debian/awstats.conf \# Install (and cleanup) java applet \# TODO:
use DEB\_DH\_INSTALL\_ARGS\_awstats when some day implemented in CDBS
DEB\_DH\_LINK\_awstats =
usr/share/java/awstats/awgraphapplet\$(DEB\_NOEPOCH\_VERSION).jar
usr/share/java/awstats/awgraphapplet.jar binary-install/awstats::
DEB\_DH\_INSTALL\_ARGS =
wwwroot/classes/awgraphapplet\$(DEB\_NOEPOCH\_VERSION).jar
usr/share/java/awstats/ clean:: rm -f
wwwroot/classes/awgraphapplet?\*.jar \# Remove badly coded PDF and
superfluous GPL license texts binary-post-install/awstats:: find
\$(DEB\_DESTDIR) -type f \\ \\( -name '\*.pdf' -o -name COPYING.TXT -o
-name LICENSE.TXT \\) \\ -exec rm '{}' + \# Set scripts executable - and
unset other files binary-fixup/awstats:: find
\$(DEB\_DESTDIR)/usr/share/awstats -type f -exec chmod -x '{}' + find
\$(DEB\_DESTDIR)/usr/share/awstats/tools
\$(DEB\_DESTDIR)/usr/share/doc/awstats/examples \\ \\( -name '\*.pl' -o
-name '\*.sh' \\) -exec chmod +x '{}' +

Ruby (with debhelper)
---------------------

libfeedtools-ruby
package
`debian/rules`: \#!/usr/bin/make -f \# -\*- mode: makefile; coding:
utf-8 -\*- include /usr/share/cdbs/1/rules/debhelper.mk include
/usr/share/ruby-pkg-tools/1/class/ruby-setup-rb.mk
DEB\_RUBY\_CONFIG\_ARGS += --shebang=never \$(patsubst
%,install/%,\$(DEB\_RUBY\_REAL\_LIB\_PACKAGES)) :: install/% : chmod a+x
debian/\$(cdbs\_curpkg)/\$(DEB\_RUBY\_LIBDIR)/feed\_tools/vendor/html5/bin/\*
sed -i "s/\^\#!\\/usr\\/bin\\/env \*ruby\$\$/\#!\\/usr\\/bin\\/env
ruby\$(cdbs\_ruby\_ver)/"
debian/\$(cdbs\_curpkg)/\$(DEB\_RUBY\_LIBDIR)/feed\_tools/vendor/html5/bin/\*

GNOME (uses autotools)
----------------------

### gnome-panel package

`debian/rules`: \#!/usr/bin/make -f include
/usr/share/cdbs/1/rules/debhelper.mk include
/usr/share/cdbs/1/rules/utils.mk include
/usr/share/cdbs/1/class/gnome.mk include
/usr/share/gnome-pkg-tools/1/rules/uploaders.mk include
/usr/share/gnome-pkg-tools/1/rules/gnome-version.mk -include
/usr/share/gnome-pkg-tools/1/rules/gnome-get-source.mk LDFLAGS +=
-Wl,-z,defs -Wl,-O1 -Wl,--as-needed DEB\_CONFIGURE\_EXTRA\_FLAGS += \\
--disable-scrollkeeper \\ --with-in-process-applets=all ifneq
(\$(DEB\_BUILD\_GNU\_SYSTEM),gnu) DEB\_CONFIGURE\_EXTRA\_FLAGS +=
--enable-eds endif DEB\_DH\_MAKESHLIBS\_ARGS\_libpanel-applet2-0 +=
-V"libpanel-applet2-0 (\>= 2.28.0)"
DEB\_DH\_MAKESHLIBS\_ARGS\_gnome-panel += --no-act
binary-install/gnome-panel:: chmod a+x
debian/gnome-panel/usr/lib/gnome-panel/\*
binary-install/gnome-panel-data:: find debian/gnome-panel-data/usr/share
-type f -exec chmod -R a-x {} \\; cd
debian/gnome-panel-data/usr/share/gconf && \\ mkdir defaults && \\ mv
schemas/panel-default-setup.entries \\
defaults/05\_panel-default-setup.entries
binary-install/libpanel-applet2-doc:: find
debian/libpanel-applet2-doc/usr/share/doc/libpanel-applet2-doc/ -name
".arch-ids" -depth -exec rm -rf {} \\;

### gdm3 package

`debian/rules`: \#!/usr/bin/make -f include
/usr/share/cdbs/1/rules/debhelper.mk include
/usr/share/cdbs/1/class/gnome.mk include
/usr/share/gnome-pkg-tools/1/rules/uploaders.mk include
/usr/share/gnome-pkg-tools/1/rules/gnome-version.mk include
/usr/share/gnome-pkg-tools/1/rules/patch-translations.mk -include
/usr/share/gnome-pkg-tools/1/rules/gnome-get-source.mk GNOME\_MODULE :=
gdm DEB\_CONFIGURE\_SCRIPT\_ENV += X\_PATH="/usr/bin" \\
X\_SERVER\_PATH="/usr/bin" \\ X\_SERVER="/usr/bin/Xorg" ifeq
(linux,\$(DEB\_HOST\_ARCH\_OS)) DEB\_CONFIGURE\_SCRIPT\_ENV +=
X\_CONFIG\_OPTIONS="-audit 0 -novtswitch" else
DEB\_CONFIGURE\_SCRIPT\_ENV += X\_CONFIG\_OPTIONS="-audit 0" endif
DEB\_CONFIGURE\_EXTRA\_FLAGS += --disable-scrollkeeper \\
--enable-ipv6=yes \\ --with-at-spi-registryd-directory=/usr/lib/at-spi
\\ --with-default-path=/usr/local/bin:/usr/bin:/bin:/usr/games \\
--with-custom-conf=/etc/gdm3/daemon.conf \\ --with-sysconfsubdir=gdm3 \\
--with-working-directory=/var/lib/gdm3 \\ --with-xauth-dir=/var/run/gdm3
\\ --with-pid-file=/var/run/gdm3.pid \\ --with-log-dir=/var/log/gdm3 \\
--with-screenshot-dir=/var/run/gdm3/greeter \\
--with-defaults-conf=/usr/share/gdm/defaults.conf \\
--with-user=Debian-gdm --with-group=Debian-gdm \\ --with-pam-domain=gdm3
DEB\_MAKE\_EXTRA\_ARGS += authdir=/var/lib/gdm3 ifeq
(linux,\$(DEB\_HOST\_ARCH\_OS)) DEB\_CONFIGURE\_EXTRA\_FLAGS +=
--with-selinux endif DEB\_DH\_INSTALLINIT\_ARGS := --noscripts
binary-install/gdm3:: chmod 755
debian/gdm3/usr/share/gdm/gdmXnestWrapper mv
debian/gdm3/usr/share/applications/gdmsetup.desktop \\
debian/gdm3/usr/share/gdm/applications/ rmdir
debian/gdm3/usr/share/applications mv debian/gdm3/usr/sbin/gdm-binary
debian/gdm3/usr/sbin/gdm3 cd debian/gdm3/usr/sbin && rm -f gdm-restart
gdm-stop gdm-safe-restart chmod 755 debian/gdm3/etc/gdm3/Xsession
dh\_installpam -pgdm3 --name=gdm3-autologin rm -rf
debian/gdm3/var/lib/gdm3/.g\* rm -rf debian/gdm3/var/run rm -f
debian/gdm3/usr/sbin/gdm rm -f debian/gdm3/etc/pam.d/gdm rm -f
debian/gdm3/etc/pam.d/gdm-autologin cd
debian/gdm3/usr/share/gdm/greeter-config && \\ mv session-setup.entries
10\_upstream.entries INFILES := \$(wildcard debian/\*.desktop.in)
OUTFILES := \$(INFILES:.desktop.in=.desktop) %.desktop: %.desktop.in
intltool-merge -d debian/po-up \$\< \$@ build/gdm3:: \$(OUTFILES)
MANPAGES := \$(patsubst %.pod,%,\$(wildcard debian/\*.pod))
common-build-arch:: \$(MANPAGES) clean:: rm -f \$(MANPAGES) rm -f
\$(OUTFILES) %: %.pod pod2man --section=\$(shell echo \$@ | sed
's/.\*\\.//') \\ --release="GNOME \$(DEB\_GNOME\_VERSION)" \\
--center="Debian GNU/Linux" \\ \$\< \\ | sed -e 's/debian:://' \>\$@

Python (with debhelper)
-----------------------

pycairo
package
`debian/rules`: \#!/usr/bin/make -f DEB\_PYTHON\_SYSTEM=pysupport
DEB\_COMPRESS\_EXCLUDE= .js .inv include
/usr/share/cdbs/1/rules/debhelper.mk include
/usr/share/cdbs/1/class/python-distutils.mk
DEB\_PYTHON\_INSTALL\_ARGS\_ALL += --install-layout=deb
install/python-cairo-dev:: python setup.py install\_data
--install-dir=\$(CURDIR)/debian/tmp/usr sphinx-build -bhtml doc
build/html build/python-cairo-dbg:: set -e; \\ for i in
\$(cdbs\_python\_build\_versions); do \\ python\$\$i-dbg ./setup.py
build; \\ done install/python-cairo-dbg:: for i in
\$(cdbs\_python\_build\_versions); do \\ python\$\$i-dbg ./setup.py
install --install-layout=deb --root \$(CURDIR)/debian/tmp; \\ done
clean:: -for i in \$(cdbs\_python\_build\_versions); do \\
python\$\$i-dbg ./setup.py clean -a; \\ done rm -f \$(CURDIR)/pycairo.pc
\$(CURDIR)/src/config.h rm -rf \$(CURDIR)/build

Simple Makefile (with dpatch)
-----------------------------

apg
package
`debian/rules`: \#!/usr/bin/make -f \# to re-generate debian/control,
invoke \# fakeroot debian/rules debian/control
DEB\_AUTO\_UPDATE\_DEBIAN\_CONTROL:=yes \# automatic debian/control
generation disabled, cdbs bug \#311724. DEB\_MAKE\_CLEAN\_TARGET :=
clean DEB\_MAKE\_BUILD\_TARGET := standalone DEB\_MAKE\_INSTALL\_TARGET
:= install INSTALL\_PREFIX=\$(CURDIR)/debian/apg/usr include
/usr/share/cdbs/1/rules/debhelper.mk include
/usr/share/cdbs/1/rules/dpatch.mk include
/usr/share/cdbs/1/class/makefile.mk cleanbuilddir/apg:: rm -f
build-stamp configure-stamp php.tar.gz install/apg:: mv
\$(CURDIR)/debian/apg/usr/bin/apg \$(CURDIR)/debian/apg/usr/lib/apg/apg
tar --create --gzip --file php.tar.gz --directory
\$(CURDIR)/php/apgonline/ . install -D --mode=0644 php.tar.gz
\$(CURDIR)/debian/apg/usr/share/doc/apg/php.tar.gz rm php.tar.gz install
-D --mode=0755 \$(CURDIR)/debian/apg.wrapper
\$(CURDIR)/debian/apg/usr/bin/apg install -D --mode=0644
\$(CURDIR)/debian/apg.conf \$(CURDIR)/debian/apg/etc/apg.conf

Perl (with upstream-tarball + utils + debhelper)
------------------------------------------------

libfuse-perl
package
`debian/rules`: \#!/usr/bin/make -f include
/usr/share/cdbs/1/rules/upstream-tarball.mk include
/usr/share/cdbs/1/rules/utils.mk include
/usr/share/cdbs/1/class/perl-build.mk include
/usr/share/cdbs/1/rules/debhelper.mk DEB\_UPSTREAM\_PACKAGE =
NetSDS-Kannel \#DEB\_UPSTREAM\_URL =
http://www.cpan.org/modules/by-module/NetSDS DEB\_UPSTREAM\_URL =
http://search.cpan.org/CPAN/authors/id/R/RA/RATTLER
DEB\_UPSTREAM\_TARBALL\_MD5 = 71428ec66538cb4384d47d9a29b12230 \#
supress regression tests, currently too broken DEB\_PERL\_CHECK\_TARGET
=

Useful tools
============

`cdbs-edit-patch` (provided with CDBS)
--------------------------------------

This script is intended to help lazy people edit or easily create
patches for the simple-patchsys patch system.

Invoke this script with the name of the patch as argument, and you will
enter a copy of your work directory in a subshell where you can edit
sources. When your work is done and you are satisfied with your changes,
just exit the subshell and you will get back to normal world with
`debian/patches/.patch` created or modified accordingly. The script
takes care to apply previous patches (ordered patches needed !), current
patch if already existing (in case you want to update it), then generate
an incremental diff to only get desired modifications. If you want to
cancel the patch creation / modification, you only need to exit the
subshell with a non-zero value and the diff will not be generated (only
cleanups will be done).

Conclusion
==========

CDBS solves most common problems and is very pleasant to use. More and
more DD are using it, not because they are obliged to, but because they
tasted and found it could improve their packages and avoid loosing time
on designing silly and complicated rules.

CDBS is not perfect, the BTS entry is not clear, but fixing a single bug
most of the time fix a problem for plenty of other packages. CDBS is not
yet capable of handling very complicated situations (like packages where
multiple C/C++ builds with different options and/or patches are
required), but this only affects a very small number of packages. These
limitations would be solved in CDBS2, which is work in progress (please
contact Jeff Bailey <jbailey@raspberryginger.com> if you want to help).

Using CDBS more widely would improve Debian's overall quality. Don't
hesitate trying it, talking to your friends about it, and contributing.

Have a Lot of FUN with CDBS !!! :-)

Thanks to Jeff for his patience and for replying my so many questions.

Special thanks to GuiHome for his help to review this documentation.

This document is a [DocBook](http://docbook.org/) application, checked
using xmllint (from [libxml2](http://www.xmlsoft.org/)), produced using
xsltproc (from [libxslt](http://xmlsoft.org/XSLT/)), using the [N.
Walsh](http://nwalsh.com/) and
[dblatex](http://dblatex.sourceforge.net/) XLST stylesheets, and
converted with [LaTeX](http://www.latex-project.org/) tools (latex,
mkindex, pdflatex & dvips) /
[pstotext](http://research.compaq.com/SRC/virtualpaper/pstotext.html)
(with [GS](http://www.cs.wisc.edu/~ghost/)).

[^1]: /!\\ deprecated, only defined to provide backward compatibility
    /!\\ (see man dpkg-architecture for more information)

[^2]: automatically set by GNU make.

[^3]: see the [custom rules below](#common-targets).

[^4]: -arch and -indep variants are defined too, and follow the same
    recipe with the corresponding -indep and -arch targets ignored

[^5]: see [Debian Policy
    §4.9](http://www.debian.org/doc/debian-policy/ch-source.html#s-debianrules).

[^6]: if we are building in the source directory, a broken upstream
    build system would leave dirty things behind, and you will have to
    handle this yourself.

[^7]: for compat level \<7.

[^8]: for compat level \>=7.

[^9]: in compat level \>=7, debhelper automatically looks for files in
    `debian/tmp`.

[^10]: not called for debug packages.

[^11]: generally not needed, unless your package builds multiple flavors
    of the same library.

[^12]: generally not needed, unless your package builds multiple flavors
    of the same library or the library is installed into a directory not
    in the regular library search path.

[^13]: needed to automatically handle bytecompilation for these modules.

[^14]: generate and install Rdoc documentation
