Specific Tasks
==============

Packaging applications with a generic build-system
--------------------------------------------------

### Class for Makefile

This class is for the guys who only have a Makefile (no autotools
available) to build the program. You only need to have four rules in the
Makefile:

-   one for cleaning the build directory (i.e. mrproper)

-   one for building your software (i.e. myprog)

-   one for checking if the software is working properly (i.e. check)

-   one for installing your software (i.e. install)

To be honest, the install rules is not a must-have, but it always helps
a lot when you've got it.

#### Classic Build

The first operation, is to write the `debian/rules`. First, we add the

    include lines: include /usr/share/cdbs/1/class/makefile.mk

Now, it remains to tell cdbs the name of our four Makefile rules. For
the previous examples it gives: 

    DEB\_MAKE\_CLEAN\_TARGET := mrproper
    \# if you detect authors's loss of sanity, tell CDBS not to try running the
    \# nonexistent clean rule, and do the job yourself in `debian/rules`
    DEB\_MAKE\_CLEAN\_TARGET := DEB\_MAKE\_BUILD\_TARGET := myprog
    DEB\_MAKE\_INSTALL\_TARGET := install DESTDIR=\$(CURDIR)/debian/tmp/
    \# no check for this software
    DEB\_MAKE\_CHECK\_TARGET := 
    
    \# allow changing the makefile filename in case of emergency exotic practices
    DEB\_MAKE\_MAKEFILE := MaKeFiLe 
    \# example when changing environment variables is necessary:
    DEB\_MAKE\_ENVVARS := CFLAGS="-fomit-frame-pointer"

`DEB_BUILD_OPTIONS` is checked for the following options:

-   noopt: use -O0 instead of -O2

-   nocheck: skip the check rule

If your Makefile doesn't support the `DESTDIR` variable, take a look in
it and find the variable responsible for setting installation directory.
If you don't find some variable to do this, you'll have to patch the
file...

That's all :).

#### Multiple "flavors" Build

After setting the basic parameters like for a classic build, you just
need to specify the list of your "flavors" and ensure you've got a
specific build directory (which can't be the same as the source
directory or all builds would walk over themselves in a big mess):

    \#needs to be declared before including makefile.mk 
    DEB\_MAKE\_FLAVORS = full nox light 
    DEB\_BUILDDIR = build

If you need to create customized rules like this:

    \$(cdbs\_make\_clean\_nonstamps):: 
        \$(if \$(cdbs\_make\_flavors),rm -f extra-stuff-\$(cdbs\_make\_curflavor))
        
    \$(cdbs\_make\_install\_stamps)::
        \$(if \$(cdbs\_make\_flavors),dostuff --with=\$(cdbs\_make\_curflavor))
        
you can use these extra CDBS variables:

-   `cdbs_make_flavors`: sorted flavor list (nonempty if multibuild
    activated)

-   `cdbs_make_curflavor`: current flavor for this build

-   `cdbs_make_curbuilddir`: build directory for the current flavor

-   `cdbs_make_curdestdir`: installation directory for the current
    flavor

### Class for Autotools

This class is able to use configure scripts and makefiles generated with
autotools (and possibly libtool). All rules are called automatically and
clean rules to remove generated files during build are also added. This
class in fact improves the makefile class to support autotools features
and provide good defaults.

#### Classic Build

To use it, just add this line to your `debian/rules` include
/usr/share/cdbs/1/class/autotools.mk

CDBS automatically handles common flags to pass to the configure script,
but it is possible to give some extra parameters:

    DEB\_CONFIGURE\_EXTRA\_FLAGS := --with-ipv6 --with-foo

If the build system uses non-standard configure options you can override
CDBS default behavior: 

    COMMON\_CONFIGURE\_FLAGS := --program-dir=/usr
    
(notice that `DEB_CONFIGURE_EXTRA_FLAGS` would still be appended)

If some specific environment variables need to be setup, use:

    DEB\_CONFIGURE\_SCRIPT\_ENV += BUILDOPT="someopt"

> **Warning**
>
> Prefer use of += instead of := not to override other environment
> variables (CC / CXX / CFLAGS / CXXFLAGS / CPPFLAGS / LDFLAGS)
> propagated in the CDBS default.

CDBS will automatically update `config.sub`, `config.guess`, and
`config.rpath` before build and restore the old ones at clean stage
(even if using the tarball system). If needed, and if `debian/control`
management is activated, autotools-dev and/or gnulib will then be
automatically added to the build dependencies (needed to find updated
versions of the files).

If the program does not use the top source directory to store autoconf
files, you can teach CDBS where it is to be found: 

    DEB\_AC\_AUX\_DIR = \$(DEB\_SRCDIR)/autoconf

CDBS can be asked to update libtool, autoconf, and automake files, but
this behavior is likely to break the build system and is *STRONGLY*
discouraged. Nevertheless, if you still want this feature, set the
following variables:

-   `DEB_AUTO_UPDATE_LIBTOOL`: `pre` to call libtoolize, or `post` to
    copy system-wide `libtool` after configure is done

-   `DEB_AUTO_UPDATE_ACLOCAL`: `aclocal` version to use

-   `DEB_AUTO_UPDATE_AUTOCONF`: `autoconf` version to use

-   `DEB_AUTO_UPDATE_AUTOHEADER`: `autoheader` version to use

-   `DEB_AUTO_UPDATE_AUTOMAKE`: `automake` version to use

-   `DEB_ACLOCAL_ARGS`: extra arguments to `aclocal` call (defaults to
    `/m4` if exists)

-   `DEB_AUTOMAKE_ARGS`: extra arguments to `automake` call

(corresponding build dependencies will automatically be added)

The following make parameters can be overridden:

    \# these are the defaults CDBS provides 
    DEB\_MAKE\_INSTALL\_TARGET := install DESTDIR=\$(DEB\_DESTDIR)
    DEB\_MAKE\_CLEAN\_TARGET := distclean
    DEB\_MAKE\_CHECK\_TARGET := 
    
    \# example to work around dirty makefile
    DEB\_MAKE\_INSTALL\_TARGET := install prefix=\$(CURDIR)/debian/tmp/usr
    
    \# example with nonexistent install rule for make
    DEB\_MAKE\_INSTALL\_TARGET := 
    
    \# example to activate check rule
    DEB\_MAKE\_CHECK\_TARGET := check 
    
    \# overriding make-only environment variables : 
    \# (should never be necessary in a clean build system) 
    \# (example borrowed from the bioapi package)
    DEB\_MAKE\_ENVVARS := "SKIPCONFIG=true"

`DEB_BUILD_OPTIONS` is checked for the following options:

-   noopt: use -O0 instead of -O2

-   nocheck: skip the check rule

If you are using CDBS version \< 0.4.39, it automatically cleans
autotools files generated during build (`config.cache`, `config.log`,
and `config.status`). Since version 0.4.39, CDBS leave them all
considering it is not his job to correct an upstream buildsys
misbehavior (but you may remove them in the clean rule if necessary
before you get the issue solved by authors).

#### Multiple "flavors" Build

The basics are the same as [in the makefile
class](#makefile-class-multibuild) (on which the autotools class
depends). Combined with this class you can do interesting customizations
like this: 

    \# global configure options
    DEB\_CONFIGURE\_EXTRA\_FLAGS = --enable-gnutls 
    
    \# per flavor configure options
    DEB\_CONFIGURE\_EXTRA\_FLAGS\_full = --enable-gtk --enable-pam
    DEB\_CONFIGURE\_EXTRA\_FLAGS\_nox = --enable-pam
    DEB\_CONFIGURE\_EXTRA\_FLAGS\_light = --disable-unicode
    DEB\_CONFIGURE\_EXTRA\_FLAGS += \$(DEB\_CONFIGURE\_EXTRA\_FLAGS\_\$(cdbs\_make\_curflavor))

### Class for CMake

This class is intended to handle build systems using
[CMake](http://www.cmake.org/).

To use this class, add this line to your `debian/rules`: 

    include /usr/share/cdbs/1/class/cmake.mk

This class is an extension of the Makefile class, calling `cmake` in
`DEB_SRCDIR` with classic parameters and then calling `make`. In the
call to `cmake`, the install prefix and path to CC/CXX as well as
CFLAGS/CXXFLAGS are given automatically. You can pass extra arguments
using `DEB_CMAKE_EXTRA_FLAGS`, and in strange cases where you need a
special configuration you can override the default arguments using
`DEB_CMAKE_NORMAL_ARGS`.

### Class for qmake

This class is intended to handle build systems using
[qmake](http://doc.trolltech.com/4.5/qmake-manual.html), mostly used to
build Qt applications.

To use this class, add this line to your `debian/rules`: 

    include /usr/share/cdbs/1/class/qmake.mk

This class is an extension of the Makefile class, calling `qmake` in
`DEB_BUILDDIR` with classic parameters and then calling `make`. In the
call to `qmake`, the path to CC/CXX as well as
CPPFLAGS/CFLAGS/CPPFLAGS/CXXFLAGS are given automatically. Configuration
options can be given using `DEB_QMAKE_CONFIG_VAL` (resulting in adding
content to `CONFIG`), the later defaulting to `nostrip` if this option
is set in `DEB_BUILD_OPTIONS`.

### Class for SCons

This class is intended to handle build systems using
[SCons](http://www.scons.org/), which is an alternative to make.

To use this class, add this line to your `debian/rules`: 

    include /usr/share/cdbs/1/class/scons.mk

The class takes care to invoke SCons and cleanup things afterwards. You
can setup a few parameters: 

    \# options for ALL targets (in addition to the DEB\_SCONS\_\*\_OPTIONS variables) 
    DEB\_SCONS\_OPTIONS = useALSA=1

    \# you can override the default build target (empty)
    DEB\_SCONS\_BUILD\_TARGET = 
    \# options for the build target
    DEB\_SCONS\_BUILD\_OPTIONS = useGettext=1 
    
    \# you can override the default install target ("install") 
    DEB\_SCONS\_INSTALL\_TARGET = install devel-install 
    \# options for the install target
    DEB\_SCONS\_INSTALL\_OPTIONS = instdir=\$(CURDIR)/debian/tmp
    
    \# you can override the default clean target (.)
    DEB\_SCONS\_CLEAN\_TARGET = fullclean 
    
    \# activate the check target if set to the target name
    DEB\_SCONS\_CHECK\_TARGET = testsuite

Packaging Perl applications
---------------------------

### Subclass for Makefile (ExtUtils::MakeMaker)

> **Warning**
>
> This class replaces the 'perlmodule' class, which is deprecated. This
> one is very similar: rules and parameters are almost the same, and
> only the `DEB_MAKEMAKER_PACKAGE` variable is deprecated, so the
> conversion should be pretty easy.

This class can manage Perl builds using
[ExtUtils::MakeMaker](http://perldoc.perl.org/ExtUtils/MakeMaker.html)
(Makefile.PL) automatically.

To use this class, add this line to your `debian/rules`: 

    include /usr/share/cdbs/1/class/perl-makemaker.mk

You can customize build options like this: 

    \# add custom MakeMaker options 
    DEB\_MAKEMAKER\_USER\_FLAGS = --with-ipv6

As this class is a subclass of the makefile class, you can override the
`DEB_MAKE_` variables, but it should not be needed. Also beware
`DEB_BUILDDIR` must match `DEB_SRCDIR` for this build system.

### Class for Module::Build

This class can manage Perl builds using
[Module::Build](http://perldoc.perl.org/Module/Build.html) (Build.PL)
automatically.

The following parameters can be overridden: 

    \# these are the defaults CDBS provides 
    DEB\_PERL\_BUILD\_TARGET = build 
    DEB\_PERL\_CHECK\_TARGET = test 
    DEB\_PERL\_INSTALL\_TARGET = install
    DEB\_PERL\_CLEAN\_TARGET = realclean 
    
    \# add your custom parameters if the defaults do not suits you
    DEB\_PERL\_CHECK\_FLAGS = fulltestsuite=1 
    DEB\_PERL\_CONFIGURE\_FLAGS = destdir=\$(CURDIR)/build/stuff

Also beware `DEB_BUILDDIR` must match `DEB_SRCDIR` for this build
system.

### Customizations common to all Perl classes

If you included the debhelper class, it can take care of calling
`dh_perl`. Have a look at Perl-specific debhelper options described
[here](#debhelper-commands).

Extra variables you can use in your `debian/rules`:

-   `DEB_PERL_PACKAGES`: list of Perlpackages

-   `DEB_PERL_ARCH_PACKAGES`: list of architecture dependent
    Perlpackages

-   `DEB_PERL_INDEP_PACKAGES`: list of architecture independent
    Perlpackages

> **Important**
>
> If `debian/control` management is activated (see below), build
> dependency on perl is automatically added, if not, you will have to do
> it yourself.

Packaging Python applications
-----------------------------

With the new policy all versioned packages (pythonver-app) are collapsed
into a single package (python-app). The Python classes are able to move
python scripts and .so files in the new locations automatically. You can
use the auto control file generation feature to ensure your
Build-Depends are set correctly for the new needed tools. The classes
also remove `.egg-info` directories sometimes left over by Python
distutils.

Before choosing the right class to use (depending on the build system
used upstream, and described in the next sections), you need to choose a
Python package manager. You may wonder why this choice at all, and why
Debian Python folks did not stick to a single common tool, but this is a
long tragedy, and you'd find most of it on the Debian mailing-lists, if
you're curious. You can find more information on these tools on [this
page](http://wiki.debian.org/DebianPythonFAQ) and in the corresponding
packages.

Then, add the following *before* the Python class inclusion to your
`debian/rules`: 

    \# select the python system you want to use : pysupport or pycentral 
    \# (this MUST be done before including the class)
    DEB\_PYTHON\_SYSTEM = pysupport

It can handle the following debhelper commands automagically:

  Commands                                                                        Parameters
  ------------------------------------------------------------------------------- ---------------------------------------------------------- -------------------------------------------------------
  **commands called during 'binary-post-install/package' rules, in this order**
  `dh_pysupport` or `dh_pycentral`.                                               `DEB_PYTHON_PRIVATE_MODULES_DIRS`[^13]                     list of private modules directories, for all packages
  `DEB_PYTHON_PRIVATE_MODULES_DIRS_`                                              list of private modules directories, for package package

  : Debhelper commands managed for Python packaging

### Class for Python distutils

This class can manage common Python builds using
[distutils](http://docs.python.org/distutils/) automatically.

> **Warning**
>
> Since 0.4.53, this class does not handle old-policy packages
> (pre-Etch) anymore.

To use this class, add these lines to your `debian/rules`: \# Don't
forget to declare DEB\_PYTHON\_SYSTEM before this line include
/usr/share/cdbs/1/class/python-distutils.mk

You can customize build options like this: 

    \#\#\#\#\#\#\#\#\#\#\#\#\#\# These variables have not changed (same in the old and new policy) 
    \# change the Python build script name (default is 'setup.py')
    DEB\_PYTHON\_SETUP\_CMD := install.py 
    
    \# clean options for the Python build script
    DEB\_PYTHON\_CLEAN\_ARGS = -all 
    
    \# build options for the Python build script
    DEB\_PYTHON\_BUILD\_ARGS = --build-base="\$(DEB\_BUILDDIR)/specific-build-dir" 
    
    \# common additional install options for all binary packages
    \#   ('--root' option is always set)
    DEB\_PYTHON\_INSTALL\_ARGS\_ALL = --no-compile --optimize --force

Complete `debian/rules` example using python-support for a module
(editobj): 

    \#!/usr/bin/make -f 
    \# -\*- mode: makefile; coding: utf-8 -\*- 

    include /usr/share/cdbs/1/rules/debhelper.mk     
    DEB\_PYTHON\_SYSTEM = pysupport
    include /usr/share/cdbs/1/class/python-distutils.mk 
    include /usr/share/cdbs/1/rules/patchsys-quilt.mk
    
    DEB\_COMPRESS\_EXCLUDE := .py
    
    \$(patsubst %,install/%,\$(cdbs\_python\_packages))::
        mv debian/\$(cdbs\_curpkg)/usr/lib/python\*/site-packages/editobj/icons \\
            debian/\$(cdbs\_curpkg)/usr/share/\$(cdbs\_curpkg) \$(patsubst

    %,binary-install/%,\$(cdbs\_python\_packages))::
        find debian/\$(cdbs\_curpkg)/usr/share/ -type f -exec chmod -R a-x {} \\;

### Subclass for autotools

For a Python module using autotools, it may be convenient to use this
class, which is a subclass of the generic autotools class. It currently
complement the `DEB_CONFIGURE_SCRIPT_ENV` variable with the `PYTHON`
parameter, and adds needed build dependencies if `debian/control`
management is activated. Moreover, if the package is to be built for
multiple Python versions, then the multiple "flavors" build is
automatically activated (`DEB_MAKE_FLAVORS` is set accordingly, but
still can be overridden is needed).

To use this class, add this line to your `debian/rules`:

    include /usr/share/cdbs/1/class/python-autotools.mk

### Class for Python Sugar

This class can manage activity extensions for the [Sugar Learning
Platform](http://sugarlabs.org/) (originally developed for the One
Laptop per Child XO-1 netbook) automatically.

To use this class, add these lines to your `debian/rules`:

    include /usr/share/cdbs/1/class/python-sugar.mk
    
    \# Compulsory: Sugar branches supported by this activity extension
    DEB\_SUGAR\_BRANCHES = 0.84 0.86

### Customizations common to all Python classes

You can customize build options like this:

    \#\#\#\#\#\#\#\#\#\#\#\#\#\# These variables are additions for the new policy 
    
    \# packages holding the collapsed content of all supported versions
    \# CDBS defaults to the first non -doc/-dev/-common package listed in "debian/control"
    \# this variable allows to override automatic detection
    DEB\_PYTHON\_MODULE\_PACKAGES = mypyapp 
    
    \# list of private modules directories (for all binary packages) (needed to automatically handle
        bytecompilation)
    DEB\_PYTHON\_PRIVATE\_MODULES\_DIRS = /usr/share/mypyapp/my-pv-module 
    
    \# or for a specific binary package
    DEB\_PYTHON\_PRIVATE\_MODULES\_DIRS\_python-stuff-client = /usr/share/mypyapp/client-pv-module 

    \# overrides the default Python installation root directory 
    DEB\_PYTHON\_DESTDIR = \$(CURDIR)/debian/python-stuff

You may use some read-only (meaning you MUST NOT alter them) variables
in your `debian/rules`:

-   `cdbs_python_current_version`: current python version number
    (defined only if selected package is a module)

-   `cdbs_python_build_versions`: list of space separated version
    numbers for which the selected module/extension is gonna be built

-   `cdbs_python_destdir`: Python installation root directory (works
    even when you didn't used `DEB_PYTHON_DESTDIR`

-   `cdbs_python_packages`: list of all Python packages

-   `cdbs_python_arch_packages`: list of all Python architecture
    dependent packages

-   `cdbs_python_indep_packages`: list of all Python architecture
    independent packages

> **Warning**
>
> Do not use the `DEB_PYTHON_MODULE_PACKAGE` variable anymore, it has
> been obsoleted. Use the `DEB_PYTHON_MODULE_PACKAGES` to force the list
> of module packages, or `cdbs_python_packages` variable in rules
> generation.
>
> Do not use the `cdbs_python_support_path` and
> `cdbs_python_module_arch` variables anymore, they have been obsoleted.
> `cdbs_python_support_path` is not useful anymore, just install files
> in the standard way and python-support would do the rest (messing into
> python-support internals was only a workaround when it was
> incomplete). `cdbs_python_module_arch` can easily be replace by a rule
> rewrite or a membership test using `cdbs_python_arch_packages` or
> `cdbs_python_indep_packages`.

Packaging Ruby applications
---------------------------

### Class for Ruby setup.rb

This class can manage common setup.rb installer automatically.

To use this class, install the ruby-pkg-tools package, and add this line
to your `debian/rules`: 

    include /usr/share/ruby-pkg-tools/1/class/ruby-setup-rb.mk

It can handle the following debhelper commands automagically:

  Commands                                                                   Parameters
  -------------------------------------------------------------------------- ------------ --
  **commands called during 'binary-install/package' rules, in this order**
  `dh_rdoc`[^14]                                                                          

  : Debhelper commands managed for Ruby packaging

Most ruby packages are architecture all, and then don't need being build
for multiple ruby versions; your package should then be called
'libfoo-ruby' or 'foo' and CDBS would automatically use the current
Debian ruby version to build it. If your package contains a compiled
part or a binding to an external lib, then you will have packages named
'libfoo-ruby1.6', 'libfoo-ruby1.8', and so on, then CDBS would
automatically build each package with the corresponding ruby version. In
this case, don't forget to add a 'libfoo-ruby' convenience dummy package
depending on the current Debian ruby version. If you have documentation
you want split into a separate package, then call it 'libfoo-ruby-doc'.
If this is Rdoc documentation, you may want to include the debhelper
class, as explained before, to have it generated and installed
automagically.

You can customize build options like this: 

    \# force using a specific ruby version for build 
    \# (should not be necessary) 
    DEB\_RUBY\_VERSIONS := 1.9 
    
    \# use ancestor 
    DEB\_RUBY\_SETUP\_CMD := install.rb 
    
    \# config options for the ruby build script 
    \# (older setup.rb used --site-ruby instead of --siteruby) 
    DEB\_RUBY\_CONFIG\_ARGS = --site-ruby=/usr/lib/ruby/1.9-beta

### Rules for the Ruby Extras Team

> **Warning**
>
> The uploaders rule is *deprecated*. It has been decided Uploaders
> should only be people who made significant changes to the package (and
> then added manually). Read [this
> thread](http://lists.alioth.debian.org/pipermail/pkg-ruby-extras-maintainers/2007-February/001426.html)
> for more information.

If you are part of the Ruby Extras Team, or having the Team as
Uploaders, and you feel bored maintaining the list of developers, this
rule is made for you.

To use this class, install the ruby-pkg-tools package, and add this line
to your `debian/rules`: include
/usr/share/ruby-pkg-tools/1/rules/uploaders.mk

Rename your `debian/control` file to `debian/control.in` and run the
clean rule (`./debian/rules clean`) to regenerate the `debian/control`
file, replacing the `@RUBY_EXTRAS_TEAM@` (`@RUBY_TEAM@` is deprecated)
tag with the list of developers automatically.

Packaging GNOME applications
----------------------------

### Class for GNOME

> **Important**
>
> This class is intended to make packaging GNOME software easily,
> managing specific actions for you. This documentation describe the
> GNOME class, which is shipped with CDBS, but if you intend to work on
> this area seriously, you should also have a look at the GNOME Team
> rules in the next chapter.

This class adds a make environment variable : 
GCONF\_DISABLE\_MAKEFILE\_SCHEMA\_INSTALL = 1 (''This is necessary
because the Gconf schemas have to be registered at install time. In the
case of packaging, this registration cannot be done when building the
package, so this variable disable schema registration in `make install`.
This procedure if deferred until gconftool-2 is called in
`debian/postinst` to register them, and in `debian/prerm` to unregister
them. The `dh_gconf` script is able to add the right rules automatically
for you.'')

It can handle the following debhelper commands automagically:

  Commands                                                                   Parameters
  -------------------------------------------------------------------------- ---------------------------- ---------------------------------------
  **commands called during 'binary-install/package' rules, in this order**
  `dh_scrollkeeper`                                                          `DEB_DH_SCROLLKEEPER_ARGS`   extra arguments passed to the command
  `dh_gconf`                                                                 `DEB_DH_GCONF_ARGS`          extra arguments passed to the command
  `dh_icons`                                                                 `DEB_DH_ICONS_ARGS`          extra arguments passed to the command

  : Debhelper commands managed for GNOME packaging

Moreover it adds some more clean rules to remove:

-   intltool generated files

-   scrollkeeper generated files (left over `.omf.out` files in `doc`
    and `help` directories)

To use it, just add this line to your `debian/rules`, after the
debhelper class include: include /usr/share/cdbs/1/class/gnome.mk

For more information on GNOME specific packaging rules, look at the
[Debian GNOME packaging
policy](http://alioth.debian.org/docman/view.php/30194/18/gnome-policy-20030502-1.html).

### Rules for the GNOME Team

If you install the extra gnome-pkg-tools package, you will have extra
rules and tools used by the GNOME Team for their packages. You should
really have a look to the package documentation for additional
information (mostly in
`/usr/share/doc/gnome-pkg-tools/README.Debian.gz`).

If you are part of the GNOME Team, or having the Team as Uploaders, and
you feel bored maintaining the list of uploaders, add this line to your
`debian/rules`: 

    include /usr/share/gnome-pkg-tools/1/rules/uploaders.mk

Rename your `debian/control` file to `debian/control.in` and run the
clean rule (`./debian/rules clean`) to regenerate the `debian/control`
file, replacing the '@GNOME\_TEAM@' tag with the list of developers
automatically.

> **Warning**
>
> If you are using the `debian/control` file management, please note
> this rule will override this feature To cope with this problem,
> allowing at least Build-Depends handling, use the following
> work-around (until it is solved in a proper way): 
>   ifneq (, \$(DEB\_AUTO\_UPDATE\_DEBIAN\_CONTROL))
>   clean:: 
>    sed -i "s/@cdbs@/\$(CDBS\_BUILD\_DEPENDS)/g" debian/control 
>   endif

Packaging KDE applications
--------------------------

> **Warning**
>
> This class is intended for KDE 3 only. If you want to package KDE 4
> components, please refer to the pkg-kde-tools package instead, which
> is maintained by the KDE Team (and especially read the notes in
> `/usr/share/doc/pkg-kde-tools/README.Debian`); we should describe it
> in this documentation later.

To use this class, add this line to your `debian/rules` file: 

    include /usr/share/cdbs/1/class/kde.mk

CDBS automatically exports the following variables with the right value:

-   kde\_cgidir (/usr/lib/cgi-bin)

-   kde\_confdir (/etc/kde3)

-   kde\_htmldir (/usr/share/doc/kde/HTML)

`DEB_BUILDDIR`, `DEB_AC_AUX_DIR` and `DEB_CONFIGURE_INCLUDEDIR` are set
to KDE defaults.

The following files are excluded from compression:

-   .dcl

-   .docbook

-   -license

-   .tag

-   .sty

-   .el

(take care of them if you override the `DEB_COMPRESS_EXCLUDE` variable)

It can handle configure options specific to KDE (not forgetting
disabling rpath and activating xinerama), set the correct autotools
directory, and launch make rules adequately.

You can enable APIDOX build by setting the `DEB_KDE_APIDOX` variable to
a non-void value.

you can enable the final mode build by setting `DEB_KDE_ENABLE_FINAL`
variable to a non-void value.

`DEB_BUILD_OPTIONS` is checked for the following options:

-   noopt: disable optimisations (and KDE final mode, overriding
    `DEB_KDE_ENABLE_FINAL`)

-   nostrip: enable KDE debug (and disable KDE final mode, overriding
    `DEB_KDE_ENABLE_FINAL`)

You can prepare the build using the 'buildprep' convenience target:
`fakeroot debian/rules buildprep` (which is in fact calling the dist
target of `admin/Makefile.common`).

Packaging Java applications
---------------------------

This class allows packaging [Java](http://www.java.com/) applications
using [Ant](http://ant.apache.org/) (a Java-based build tool).

To use this class, add this include to your `debian/rules` and set the
following variables: 

    include /usr/share/cdbs/1/class/ant.mk
    \# Set either a single (JAVA\_HOME) or multiple (JAVA\_HOME\_DIRS) java locations 
    JAVA\_HOME := /usr/lib/kaffe 
    
    \# or set JAVACMD if you don't use default '\<JAVA\_HOME\>/bin/java' path 
    \#JAVACMD := /usr/bin/java 
    \# Set Ant location
    ANT\_HOME := /usr/share/ant-cvs

You may add additional JARs like in the following example: 

    \# list of additional JAR files ('.jar' extension may be omitted) 
    \# (path must be absolute of relative to '/usr/share/java')
    DEB\_JARS := /usr/lib/java-bonus/ldap-connector adml-adapter.jar

The property file defaults to `debian/ant.properties`.

You can provide additional JVM arguments using ANT\_OPTS. You can
provide as well additional Ant command line arguments using ANT\_ARGS
(global) and/or ANT\_ARGS\_pkg (for package pkg), thus overriding the
settings in `build.xml` and the property file.

CDBS will build and clean using defaults target from `build.xml`. To
override these rules, or run the install / check rules, set the
following variables to your needs:

    \# override build and clean target
    DEB\_ANT\_BUILD\_TARGET = makeitrule
    DEB\_ANT\_CLEAN\_TARGET = super-clean
    
    \# i want install and test rules to be run
    DEB\_ANT\_INSTALL\_TARGET = install-all
    DEB\_ANT\_CHECK\_TARGET = check
    
    \# extra settings 
    DEB\_ANT\_BUILDFILE = debian/build.xml 
    \# defaults to \$(CURDIR)/debian/ant.properties 
    DEB\_ANT\_PROPERTYFILE = src/ant.properties
    DEB\_ANT\_ARGS = -Dpackage=awgraphapplet -Dversion=\$(DEB\_NOEPOCH\_VERSION)
    DEB\_ANT\_COMPILER := org.eclipse.jdt.core.JDTCompilerAdapter

`DEB_BUILD_OPTIONS` is checked for the following options:

-   noopt: set 'compile.optimize' Ant option to false

You should be able to fetch some more information on this java-based
build tool in the [Ant Apache web site](http://ant.apache.org/).

Packaging Haskell applications
------------------------------

This class allows packaging [Haskell](http://haskell.org/) applications
using [HBuild](http://hackage.haskell.org/trac/hackage/wiki/HBuild) (the
Haskell mini-distutils). You should be able to fetch some more
information on Haskell distutils in [this
thread](http://www.haskell.org/pipermail/libraries/2003-July/001239.html).

CDBS can take care of -hugs and -ghc packages: invoke `Setup.lhs`
properly for clean and install part.

To use this class, add this line to your `debian/rules`: 

    include /usr/share/cdbs/1/class/hbuild.mk
